import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import argparse
from plot import determine_x_variable_name, extract_y_variables, get_factory_estimates, non_param_columns, \
    set_rc, x_param_plot_string, plot_args, save_plot


def create_figure(x_param_name):
    set_rc()
    # fig.set(xlabel=x_param_name, ylabel="fidelity")
    plt.xlabel(x_param_plot_string[x_param_name])
    plt.ylabel("Fidelity")
    # plt.ylim((0, 1))


def set_ax(dataframe, ax):
    x_param_name = determine_x_variable_name(dataframe)
    ax.set(xlabel=x_param_plot_string[x_param_name], ylabel="Fidelity")
    ax.margins(0)


def plot_fidelity(dataframe, name, ax=None):
    """Create (but don't show) a single data set in a preconfigured figure."""

    x_param_name = determine_x_variable_name(dataframe)
    x = dataframe[x_param_name]
    avg_f, f_error, avg_t, t_error = extract_y_variables(dataframe)

    if ax is None:
        plt.errorbar(x=x, y=avg_f, yerr=f_error, label=name, **plot_args[dataframe.type[0]])
    else:
        ax.errorbar(x=x, y=avg_f, yerr=f_error, label=name, **plot_args[dataframe.type[0]])


def plot_factory_fidelity_estimate(dataframe, name, ax=None):
    x, est_f, est_t = get_factory_estimates(dataframe)
    if ax is None:
        plt.plot(x, est_f, label=name, **plot_args["factory_approximation"])
    else:
        ax.plot(x, est_f, label=name, **plot_args["factory_approximation"])


def plot_factory_fidelity_bound(dataframe, name, ax=None):
    x, est_f, est_t = get_factory_estimates(dataframe, bound=True)
    if ax is None:
        plt.plot(x, est_f, label=name, **plot_args["factory_bound"])
    else:
        ax.plot(x, est_f, label=name, **plot_args["factory_bound"])


def plot_max_mixed_line(dataframe, ax=None):
    num_remote_nodes = list(dataframe["num_remote_nodes"])
    x_param_name = determine_x_variable_name(dataframe)
    if x_param_name == "num_remote_nodes":
        max_mixed_fidelity = [1 / 2 ** n for n in num_remote_nodes]
        x = dataframe[determine_x_variable_name(dataframe)]
    else:
        x_min = dataframe[x_param_name].min()
        if x_param_name == "link_success_prob" and x_min < 0.05:
            x_min = 1e-10
        x_max = dataframe[x_param_name].max()
        x = np.linspace(x_min, x_max, 1000)
        max_mixed_fidelity = [1 / 2 ** num_remote_nodes[0]] * len(x)
    if ax is None:
        plt.plot(x, max_mixed_fidelity, label="Maximally Mixed State", **plot_args["max_mixed"])
    else:
        ax.plot(x, max_mixed_fidelity, label="Maximally Mixed State", **plot_args["max_mixed"])


def plot_single_dataset(data_path, include_factory_estimate=False, include_factory_bound=False, include_max_mixed=False,
                        include_title=True, save_path=None):
    """Plot and show a single data set."""

    if save_path is None:
        save_path = "fidelity.png"

    df = pd.read_csv(data_path)

    x_param_name = determine_x_variable_name(df)
    create_figure(x_param_name)

    plot_fidelity(dataframe=df, name=df.type[0] + " simulation")
    if include_factory_estimate:
        plot_factory_fidelity_estimate(dataframe=df, name="factory estimate")

    if include_factory_bound:
        plot_factory_fidelity_bound(dataframe=df, name="factory lower bound")

    if include_title:
        suptitle = ""
        for column in [column for column in df.columns if column not in non_param_columns]:
            suptitle += f"{column}: {df[column][0]}, "
        suptitle = suptitle[:-2]
        plt.suptitle(suptitle, size=8)

    if include_max_mixed:
        plot_max_mixed_line(dataframe=df)

    # plt.ylim(bottom=0)

    plt.legend()
    save_plot(save_path)


def plot_two_datasets(data_path_1, data_path_2, save_path=None):
    """Plot and show two datasets in the same figure."""

    if save_path is None:
        save_path = "fidelity_comparison.png"

    df_1 = pd.read_csv(data_path_1)
    df_2 = pd.read_csv(data_path_2)

    name_1 = df_1.type[0]
    name_2 = df_2.type[0]
    if name_1 == name_2:
        name_1 += "_1"
        name_2 += "_2"

    x_param_name_1 = determine_x_variable_name(df_1)
    x_param_name_2 = determine_x_variable_name(df_2)
    if x_param_name_1 != x_param_name_2:
        raise ValueError("Data sets being plotted together don't have the same x variable.")
    create_figure(x_param_name_1)

    plot_fidelity(dataframe=df_1, name=name_1)
    plot_fidelity(dataframe=df_2, name=name_2)
    # plot_max_mixed_line(dataframe=df_1)

    # plt.ylim(bottom=0)

    plt.legend()
    save_plot(save_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot fidelity of GHZ-state distribution.")
    parser.add_argument("data_path", type=str)
    parser.add_argument("--save_path", "-sp", default=None, type=str)
    parser.add_argument("--extra_data", "-ed", default=None, type=str)
    parser.add_argument("--est", "-e", action="store_true", help="Plot estimate of factory fidelity.")
    parser.add_argument("--bound", "-b", action="store_true", help="Plot lower bound on factory fidelity.")
    parser.add_argument("--maxmixed", "-mm", action="store_true", help="Plot fidelity of maximally mixed state.")
    parser.add_argument("--no_title", "-nt", action="store_true", help="Include title with used parameter values.")
    parser.add_argument("--plot", "-", action="store_true", help="Show plot (apart from saving it).")

    args = parser.parse_args()
    if args.extra_data is None:
        plot_single_dataset(data_path=args.data_path, include_factory_estimate=args.est,
                            include_factory_bound=args.bound, include_max_mixed=args.maxmixed,
                            include_title=not args.no_title, save_path=args.save_path)
    else:
        plot_two_datasets(data_path_1=args.data_path, data_path_2=args.extra_data,
                          save_path=args.save_path)

    if args.plot:
        plt.show()
