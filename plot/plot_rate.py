import pandas as pd
import matplotlib.pyplot as plt
import argparse
from plot import determine_x_variable_name, extract_y_variables, get_factory_estimates, non_param_columns, \
    x_param_plot_string, set_rc, save_plot, plot_args


def create_figure(x_param_name):
    set_rc()
    # fig.set(xlabel=x_param_name, ylabel="fidelity")
    plt.xlabel(x_param_plot_string[x_param_name])
    plt.ylabel("Rate")
    # plt.ylim((0, 1))


def set_ax(dataframe, ax):
    x_param_name = determine_x_variable_name(dataframe)
    ax.set(xlabel=x_param_plot_string[x_param_name], ylabel="Rate")
    ax.margins(0)


def plot_rate(dataframe, name, ax=None):
    """Create (but don't show) a single data set in a preconfigured figure."""

    x_param_name = determine_x_variable_name(dataframe)
    x = dataframe[x_param_name]
    avg_f, f_error, avg_t, t_error = extract_y_variables(dataframe)
    rates, rate_errors = rates_and_error_from_distribution_times(avg_t, t_error)
    if ax is None:
        plt.errorbar(x=x, y=rates, yerr=rate_errors, label=name, **plot_args[dataframe.type[0]])
    else:
        ax.errorbar(x=x, y=rates, yerr=rate_errors, label=name, **plot_args[dataframe.type[0]])


def plot_factory_distribution_time_estimate(dataframe, name, ax=None):
    x, est_f, est_t = get_factory_estimates(dataframe)
    rates, rate_errors = rates_and_error_from_distribution_times(est_t, [0] * len(est_t))
    if ax is None:
        plt.plot(x, rates, label=name, **plot_args["factory_approximation"])
    else:
        ax.plot(x, rates, label=name, **plot_args["factory_approximation"])


def plot_single_dataset(data_path, include_factory_estimate=False, include_title=True):
    """Plot and show a single data set."""

    df = pd.read_csv(data_path)

    x_param_name = determine_x_variable_name(df)
    create_figure(x_param_name)

    plot_rate(dataframe=df, name=df.type[0] + " simulation")
    if include_factory_estimate:
        plot_factory_distribution_time_estimate(dataframe=df, name="factory estimate")

    if include_title:
        suptitle = ""
        for column in [column for column in df.columns if column not in non_param_columns]:
            suptitle += f"{column}: {df[column][0]}, "
        suptitle = suptitle[:-2]
        plt.suptitle(suptitle, size=8)


def plot_two_datasets(data_path_1, data_path_2):
    """Plot and show two datasets in the same figure."""

    df_1 = pd.read_csv(data_path_1)
    df_2 = pd.read_csv(data_path_2)

    name_1 = df_1.type[0] + " simulation"
    name_2 = df_2.type[0] + " simulation"
    if name_1 == name_2:
        name_1 += "_1"
        name_2 += "_2"

    x_param_name_1 = determine_x_variable_name(df_1)
    x_param_name_2 = determine_x_variable_name(df_2)
    if x_param_name_1 != x_param_name_2:
        raise ValueError("Data sets being plotted together don't have the same x variable.")
    create_figure(x_param_name_1)

    plot_rate(dataframe=df_1, name=name_1)
    plot_rate(dataframe=df_2, name=name_2)


def rates_and_error_from_distribution_times(distribution_times, distribution_time_errors):
    """Calculate rate as a function of distribution times.

    Parameters
    ----------
    distribution_times : list of float
        List of distribution times.
    distribution_time_errors : list float
        List of errors in distribution times.
        Must have same length as `distribution_times`.

    Returns
    -------
    list of float
        List of rates.
    list of float
        List of errors in rates.

    """
    rates = [rate_from_distribution_time(distribution_time) for distribution_time in distribution_times]
    rate_errors = [rate_error_from_distribution_time_and_distribution_time_error(distribution_time,
                                                                                 distribution_time_error)
                   for distribution_time, distribution_time_error in zip(distribution_times,
                                                                         distribution_time_errors)]
    return rates, rate_errors


def rate_from_distribution_time(distribution_time):
    """Calculate rate as a function of distribution time.

    Parameters
    ----------
    distribution_time : float
        Distribution time.

    Returns
    -------
    float
        Rate.

    """
    return 1 / distribution_time


def rate_error_from_distribution_time_and_distribution_time_error(distribution_time, distribution_time_error):
    """Calculate error in rate as a function of distribution time and its error.

    1 / (T + dT) = (1 / T) (1 - dt / T) -> error is dt / T ^ 2

    Parameters
    ----------
    distribution_time : float
        Distribution time.
    distribution_time_error : float
        Error in distribution time.

    Parameters
    ----------
    float
        Error in rate.

    """
    if distribution_time_error == 0:
        return 0
    return distribution_time_error / distribution_time ** 2


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot distribution time of GHZ-state distribution.")
    parser.add_argument("data_path", type=str)
    parser.add_argument("--save_path", "-sp", default=None, type=str)
    parser.add_argument("--extra_data", "-ed", default=None, type=str)
    parser.add_argument("--est", "-e", action="store_true", help="Plot estimate of factory distribution time.")
    parser.add_argument("--no_title", "-nt", action="store_true", help="Include title with used parameter values.")
    parser.add_argument("--logy", action="store_true", help="Use log scale y axis.")
    parser.add_argument("--plot", "-", action="store_true", help="Show plot (apart from saving it).")
    args = parser.parse_args()
    if args.extra_data is None:
        plot_single_dataset(data_path=args.data_path, include_factory_estimate=args.est,
                            include_title=not args.no_title)
    else:
        plot_two_datasets(data_path_1=args.data_path, data_path_2=args.extra_data)

    plt.legend()
    if args.logy:
        plt.yscale("log")
    else:
        plt.ylim(bottom=0)

    save_path = "rate.png" if args.save_path is None else args.save_path
    save_plot(save_path)

    if args.plot:
        plt.show()
