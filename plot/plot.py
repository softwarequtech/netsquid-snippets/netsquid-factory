import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import argparse
import netsquid_factory.analytical as ana


non_param_columns = ["fidelities", "distribution_times", "number_of_samples", "type"]


def get_factory_estimates(dataframe, bound=False):
    """Get analytical estimates for distribution time and fidelity of GHZ-state distribution on factory setup.

    The parameters that were used in the simulation are extracted from the dataframe.
    The, the rate and fidelity of GHZ-state distribution on a factory setup with those parameters are calculated.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        Dataframe holding GHZ-state distribution simulation results.
        Should include the used parameters per data point.
        One of these parameters can be varied over.
        Should be the output of either `simulate_bipartite.py` or `simulate_factory.py`.
    bound : bool (optional)
        If True, a lower bound is calculated.
        If False, a leading-order approximation is calculated.

    Returns
    -------
    list of float
        X-parameter values with same range as the varied parameter of dataframe,
        but with 1000 datapoints (this finer grid makes for better plotting of analytical functions).
        An exception is made if the x-parameter is num_remote_nodes, as it can only take integer values.
        If the x-parameter is link_success_prob and the smallest value in the dataframe is smaller than 0.05,
        1E-10 is used as minimum value in the range instead.
    list of float
        Estimated values of fidelity for the different values of the x variable.
    list of float
        Estimated values of rate for the different values of the x variable.

    """
    x_param_name = determine_x_variable_name(dataframe)
    if x_param_name != "num_remote_nodes":
        x_min = dataframe[x_param_name].min()
        if x_param_name == "link_success_prob" and x_min < 0.05:
            x_min = 1e-10
        x_max = dataframe[x_param_name].max()
        x = np.linspace(x_min, x_max, 1000)
    else:
        x = dataframe[x_param_name]
    constant_parameters = {column: dataframe[column][0] for column in dataframe.columns
                           if column not in non_param_columns + [x_param_name]}
    est_f = [ana.factory_fidelity(bound=bound, **constant_parameters, **{x_param_name: x_value}) for x_value in x]
    est_t = [ana.factory_waiting_time(**constant_parameters, **{x_param_name: x_value}) for x_value in x]
    return x, est_f, est_t


def determine_x_variable_name(dataframe):
    """Determine which variable is the x variable. Returns the name of this variable, and its values.

    Function assumes there is only one varied parameter, i.e. the dataframe contains only one column with varying
    values that is not in `non_param_columns`.

    """
    df_finding_x = dataframe.drop(columns=non_param_columns)
    df_finding_x = df_finding_x.loc[:, (dataframe != dataframe.iloc[0]).any()]
    if len(df_finding_x.columns) != 1:
        raise ValueError(f"The dataframe has multiple varied x parameters, namely {df_finding_x.columns}.")
    x_param_name = df_finding_x.columns[0]

    return x_param_name


def extract_y_variables(dataframe):
    """Determine values and standard errors of the y variables, i.e. of the fidelity and the distribution time."""
    fid = [eval(f) for f in dataframe["fidelities"]]
    num = list(dataframe["number_of_samples"])
    time = [eval(t) for t in dataframe["distribution_times"]]
    avg_f = [sum(fidelities) / len(fidelities) for fidelities in fid]
    f_error = [np.std(fidelities) / np.sqrt(num) for fidelities, num in zip(fid, num)]
    avg_t = [sum(distribution_times) / len(distribution_times) for distribution_times in time]
    t_error = [np.std(distribution_times) / np.sqrt(num) for distribution_times, num in zip(time, num)]

    return avg_f, f_error, avg_t, t_error


def set_rc():
    plt.rcParams.update({
        # "text.usetex": True,
        "font.size": 16,
    })
    plt.ticklabel_format(style="sci", scilimits=(-2, 2))
    plt.margins(x=0)


def create_figure(x_param_name):
    """Create a figure and axes to use for plotting."""

    fig, (ax_fidelity, ax_distribution_time) = plt.subplots(1, 2, figsize=(20, 9))
    ax_fidelity.set_title("Fidelity", fontsize=20)
    ax_fidelity.set(xlabel=x_param_name, ylabel="fidelity")

    ax_distribution_time.set_title("Distribution Time", fontsize=20)
    ax_distribution_time.set(xlabel=x_param_name, ylabel="average number of rounds")

    return fig, (ax_fidelity, ax_distribution_time)


def plot(dataframe, ax_fidelity, ax_distribution_time, name="", include_estimates=False):
    """Create (but don't show) a single data set in a preconfigured figure."""

    if include_estimates and name != "" and name[-1] != " ":
        name += " "
    simulation_tag = "simulation" if include_estimates else ""
    estimate_tag = "estimate" if include_estimates else ""

    x_param_name = determine_x_variable_name(dataframe)
    x = dataframe[x_param_name]
    avg_f, f_error, avg_t, t_error = extract_y_variables(dataframe)

    # plot
    ax_fidelity.errorbar(x=x, y=avg_f, yerr=f_error, label=name + simulation_tag)
    ax_distribution_time.errorbar(x=x, y=avg_t, yerr=t_error, label=name + simulation_tag)

    if include_estimates:
        x, est_f, est_t = get_factory_estimates(dataframe)
        ax_fidelity.plot(x, est_f, label=name + estimate_tag)
        ax_distribution_time.plot(x, est_t, label=name + estimate_tag)


def plot_single_dataset(data_path, include_estimates=False):
    """Plot and show a single data set."""

    df = pd.read_csv(data_path)

    x_param_name = determine_x_variable_name(df)
    fig, (ax_fidelity, ax_distribution_time) = create_figure(x_param_name)

    plot(dataframe=df, ax_fidelity=ax_fidelity, ax_distribution_time=ax_distribution_time,
         include_estimates=include_estimates)

    suptitle = f"type: {df['type'][0]}"
    for column in [column for column in df.columns if column not in non_param_columns]:
        suptitle += f", {column}: {df[column][0]}"
    plt.suptitle(suptitle)

    if include_estimates:
        ax_fidelity.legend()
        ax_distribution_time.legend()

    plt.show()


def plot_two_datasets(data_path_1, data_path_2, name_1=None, name_2=None, include_estimates=False):
    """Plot and show two datasets in the same figure."""

    df_1 = pd.read_csv(data_path_1)
    df_2 = pd.read_csv(data_path_2)

    name_1 = df_1.type[0] if name_1 is None else name_1
    name_2 = df_2.type[0] if name_2 is None else name_2
    if name_1 == name_2:
        name_1 += "_1"
        name_2 += "_2"

    x_param_name_1 = determine_x_variable_name(df_1)
    x_param_name_2 = determine_x_variable_name(df_2)
    if x_param_name_1 != x_param_name_2:
        raise ValueError("Data sets being plotted together don't have the same x variable.")
    fig, (ax_fidelity, ax_distribution_time) = create_figure(x_param_name_1)

    plot(dataframe=df_1, ax_fidelity=ax_fidelity, ax_distribution_time=ax_distribution_time,
         name=name_1, include_estimates=include_estimates)
    plot(dataframe=df_2, ax_fidelity=ax_fidelity, ax_distribution_time=ax_distribution_time,
         name=name_2, include_estimates=include_estimates)

    ax_fidelity.legend()
    ax_distribution_time.legend()

    plt.show()


def save_plot(path):
    fig = plt.gcf()
    # fig.set_size_inches(10, 8)
    if path[-4:] != ".png":
        path += ".png"
    plt.savefig(path, bbox_inches="tight", dpi=300)


x_param_plot_string = {"link_success_prob": r"$q_{\mathrm{link}}$",
                       "bsm_success_prob": r"$q_{\mathrm{BSM}}$",
                       "mem_depolar_prob": r"$1 - p_{\mathrm{mem}}$",
                       "bsm_depolar_prob": r"$1 - p_{\mathrm{BSM}}$",
                       "link_depolar_prob": r"$1 - p_{\mathrm{link}}$",
                       "num_remote_nodes": r"$N$",
                       }


factory_approx_plot_args = {"color": "darkblue",
                            "linestyle": "--",
                            }
factory_bound_plot_args = {"color": "darkorange",
                           "linestyle": ":",
                           "alpha": 1,
                           }
factory_sim_plot_args = {"color": "limegreen",
                         "linestyle": "-",
                         "alpha": 0.6,
                         "marker": "D",
                         "markersize": 1.5,
                         "markerfacecolor": "darkgreen",
                         "markeredgecolor": "darkgreen",
                         "linewidth": 2,
                         }
bipartite_plot_args = {"color": "darkblue",
                       "linestyle": "-",
                       "alpha": 1.,
                       "marker": "o",
                       "markersize": 1.5,
                       "markerfacecolor": "midnightblue",
                       "markeredgecolor": "midnightblue",
                       "linewidth": .2,
                       }
bipartite_2_plot_args = {"color": "green",
                         "linestyle": "-.",
                         "alpha": 0.8,
                         # "linewidth": 3,
                         }
max_mixed_plot_args = {"color": "red",
                       "linestyle": "-",
                       "alpha": 0.8,
                       # "linewidth": 2,
                       }

plot_args = {"factory_approximation": factory_approx_plot_args,
             "factory_bound": factory_bound_plot_args,
             "factory": factory_sim_plot_args,
             "bipartite": bipartite_plot_args,
             "bipartite_2": bipartite_2_plot_args,
             "max_mixed": max_mixed_plot_args}


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot simulation results for GHZ-state distribution.")
    parser.add_argument("data_path", type=str)
    parser.add_argument("--extra_data", "-ed", default=None, type=str)
    parser.add_argument("--est", "-e", action="store_true", help="Plot estimates of distribution time and fidelity.")
    args = parser.parse_args()
    if args.extra_data is None:
        plot_single_dataset(data_path=args.data_path, include_estimates=args.est)
    else:
        plot_two_datasets(data_path_1=args.data_path, data_path_2=args.extra_data, include_estimates=args.est)
