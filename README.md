NetSquid-Factory (1.0.0)
========================

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

This code allows for the simulation of GHZ-state distribution on a symmetric star-shaped network.
It includes two types of central nodes: a `GHZFactory` (`factory.py`) and a `QuantumRouter` (`bipartite.py`).
The first can be used to distribute GHZ states by preparing the state locally and then teleporting it to a set of end nodes.
The second can be used to create Bell states between end nodes, which can then perform fusion operations locally to create a GHZ state.
Both of these protocols can be executed using the code in `protocols.py`.
Additionally, to allow for the construction of a star-shaped network using the snippet NetSquid-NetConf, a  `StarBuilder` has been included in `star_builder.py`.
Additionally, tools for simulating depolarizing quantum links for which the waiting time is geometrically distributed using the snippet NetSquid-Magic are included in `werner_magic_distribtor.py`.
Finally, analytical functions for the rate and fidelity of GHZ-state distribution using `GHZFactory` are included in `analytical.py`.

This repository has been created to support the research in the paper
"Analysis of Multipartite Entanglement Distribution using a Central Quantum-Network Node"
by Guus Avis, Filip Rozpędek and Stephanie Wehner.
If you want to refer to the repository, please cite the paper.
In the paper, the setup simulated here is explained in a lot more detail, and the analytical results are derived.
Note that what is called a ''factory node'' in the paper is here `GHZFactory`, and what is called a ''2-switch'' is here `EntanglementRouter`.

To run simulations corresponding to the paper, see the code in the folder ''simulation'' in the repository.
Simulations can be run using the python files, and simulation parameters can be changed using the YAML files.
To create plots of the output of the simulation scripts (in case there is a single varied parameter), use the scripts in the `plot` folder.
`plot.py` directly shows plots of rate and fidelity side by side.
`plot_rate.py` and `plot_fidelity.py` save a plot for the rate and the fidelity, respectively.
By adding the `--plot` flag, the figure is also shown after generating it.
The flag `--extra_data` can be used to plot two data sets in one figure for comparison (e.g. factory and router),
while the flag `--estimate` can be used to include the analytical leading-order result for the factory.


We note that a different convention for the depolarizing channel is used in this file than in the paper.
In the paper, a depolarizing parameter with param p has the action on one qubit
rho -> p rho + (1 - p) Id_2 / 2.
Here, the convention that is used is
rho -> (1 - p) rho + p Id_2 / 2.
This makes it easier to work to leading order in noise parameters.

Because of the difference in convention, there is the following mapping between parameter in the paper and parameters
used here:
link_depolar_prob = 1 - p_link
bsm_depolar_prob = 1 - p_BSM
mem_depolar_prob = 1 - p_mem

Additionally, we here don't use the parameter p_GHZ for the noise in the GHZ state as it is locally prepared.
Instead, we use the fidelity of the prepared state,
ghz_fidelity = 1 - (2 ** num_qubits - 1) / 2 ** num_qubits * p_ghz

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Documentation
-------------

How to build the docs:

First build the docs by:

```bash
make build-docs
```

This will first install any required dependencies and build the html files.

To open the built docs (using 'open' or 'xdg-open'), do:

```bash
make open-docs
```

To both build the html files and open them, do:
```bash
make docs
```

The most recent documentation of the master branch can be found [here](https://docs.netsquid.org/snippets/netsquid-factory/).

Contributors
------------

Guus Avis (g.avis@tudelft.nl)

License
-------

The NetSquid-Factory has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
