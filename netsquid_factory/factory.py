from netsquid.components import DepolarNoiseModel, IMeasureBell, QuantumProgram
from netsquid.components.qprocessor import QuantumProcessor, PhysicalInstruction
from netsquid.nodes.node import Node
from netsquid.components.instructions import Instruction
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.operators as ops
from netsquid.qubits.ketstates import BellIndex
from random import random
import itertools
import math


class GHZFactory(Node):
    """Node capable of generating (and teleporting) GHZ states.

    The total number of memory positions in the factory is `num_remote_nodes` * 2.
    A GHZ state can be generated on memory positions `num_remote_nodes` through `num_remote_nodes` * 2 - 1.
    Probabilistic Bell-state measurements can be performed on qubits i and i + `num_remote_nodes`
    for i = [0, 1, ..., `num_remote_nodes`].

    Parameters
    ----------
    name : str
        Name of the node.
    num_remote_nodes : int
        Number of remote nodes that this factory can teleport a GHZ state to.
        This is equal to the number of qubits on which a GHZ state can be produced.
        Total number of positions in memory is num_remote_nodes * 2.
    mem_depolar_prob : float
        Probability a qubit in memory depolarizes per round of entanglement generation.
    ghz_fidelity : float
        Fidelity of GHZ state produced by the factory.
    bsm_success_prob : float
        Probability that Bell-state measurement can be performed successfully.
    bsm_depolar_prob : float
        Probability that a qubit depolarizes when it partakes in a Bell-state measurement.

    """
    def __init__(self, name, num_remote_nodes, mem_depolar_prob, ghz_fidelity, bsm_success_prob, bsm_depolar_prob):
        self.add_property("num_remote_nodes", num_remote_nodes)
        self.add_property("mem_depolar_prob", mem_depolar_prob)
        self.add_property("ghz_fidelity", ghz_fidelity)
        self.add_property("bsm_success_prob", bsm_success_prob)
        self.add_property("bsm_depolar_prob", bsm_depolar_prob)
        mem_noise_models = \
            [DepolarProbNoiseModel(depolar_prob=self.properties["mem_depolar_prob"])] * num_remote_nodes + \
            [None] * num_remote_nodes
        # only put noise on the memory qubits, not on the GHZ qubits
        super().__init__(name=name,
                         qmemory=QuantumProcessor(name=f"processor_of_{name}",
                                                  num_positions=2 * num_remote_nodes,
                                                  mem_noise_models=mem_noise_models))
        ghz_depolar_prob = 2 ** num_remote_nodes * (1 - self.properties["ghz_fidelity"]) / (2 ** num_remote_nodes - 1)
        ghz_noise_model = CorrelatedDepolarNoiseModel(depolar_rate=ghz_depolar_prob, time_independent=True)
        self.qmemory.add_physical_instruction(PhysicalInstruction(InitGHZ(num_remote_nodes),
                                                                  duration=0,
                                                                  topology=[tuple((i + num_remote_nodes
                                                                                   for i in range(num_remote_nodes)))],
                                                                  q_noise_model=ghz_noise_model,
                                                                  apply_q_noise_after=True
                                                                  )
                                              )
        bsm_noise_model = DepolarNoiseModel(depolar_rate=self.properties["bsm_depolar_prob"], time_independent=True)
        self.qmemory.add_physical_instruction(PhysicalInstruction(ProbabilisticBSM(self.properties["bsm_success_prob"]),
                                                                  duration=0,
                                                                  topology=[(i, i + num_remote_nodes)
                                                                            for i in range(num_remote_nodes)],
                                                                  q_noise_model=bsm_noise_model,
                                                                  apply_q_noise_after=False
                                                                  )
                                              )


class InitGHZ(Instruction):
    """Instruction to initialize qubits in the GHZ state (|00...0> + |11...1>) / sqrt(2).

    Parameters
    ----------
    num_positions : int
        Number of memory positions which can be brought to be in a shared GHZ state with this instruction.

    """
    def __init__(self, num_positions):
        super().__init__()
        self._num_positions = num_positions

    @property
    def name(self):
        return "init_ghz"

    @property
    def num_positions(self):
        return self._num_positions

    def execute(self, quantum_memory, positions, **kwargs):
        """Initialize qubits in the GHZ state (|00...0> + |11...1>) / sqrt(2).

       Parameters
       ----------
       quantum_memory : :obj:`~netsquid.components.qmemory.QuantumMemory`
           Quantum memory to execute instruction on.
       positions : list of int, length 2
           Memory positions on which the GHZ state should be put.

        """
        qubits = qapi.create_qubits(self.num_positions)
        qapi.operate(qubits=qubits[0], operator=ops.H)
        for qubit in qubits[1:]:
            qapi.operate(qubits=[qubits[0], qubit], operator=ops.CNOT)
        quantum_memory.put(qubits=qubits, positions=positions)


class ProbabilisticBSM(IMeasureBell):
    """Perform Bell-state measurement with non-unit success probability.

     Each time this operation is performed, a coin flip is performed to determine whether the operation is successful.
     If it is, :class:`netsquid.components.instructions.IMeasureBell` is performed.
     Afterwards, the qubits that this operation was performed on are all discarded (both in case of success
     and failure).
     The primary purpose of this is to prevent large combined qubit states in simulations.
     Typically, this operation is used to affect the quantum state of qubit entangled with the qubits being measured,
     and the measured qubits themselves are of little consequence.
     In these cases, discarding the qubits afterwards has no aversive consequences.

     """

    def __init__(self, success_probability):
        super().__init__()
        self._success_probability = success_probability

    @property
    def name(self):
        return "probabilistic_bell_state_measurement"

    @property
    def num_positions(self):
        return 2

    @property
    def success_probability(self):
        return self._success_probability

    def execute(self, quantum_memory, positions, **kwargs):
        """Perform Bell-state measurement with probability of failure.

        Note that all qubits partaking in this operation are discarded afterwards.

       Parameters
       ----------
       quantum_memory : :obj:`~netsquid.components.qmemory.QuantumMemory`
           Quantum memory to execute instruction on.
       positions : list of int, length 2
           Memory positions on which the Bell-state measurement should be performed.

        Returns
        -------
        :class:`netsquid.qubits.ketstates.BellIndex` or None
            Outcome of Bell-state measurement if successful, None otherwise.

        """
        if random() <= self.success_probability:
            result = BellIndex(super().execute(quantum_memory=quantum_memory, positions=positions, **kwargs)[0])
        else:
            result = None
        qubits = quantum_memory.peek(positions)
        for qubit in qubits:
            if qubit is not None:
                qapi.discard(qubit)
        quantum_memory.pop(positions=positions, skip_noise=True)
        return result


class GHZTeleport(QuantumProgram):
    """Teleport a GHZ state, which is created locally, towards remote qubits using previously established entanglement.

    Parameters
    ----------
    num_remote_qubits : int
        Number of remote qubits which need to be put into a GHZ state.

    """
    def __init__(self, num_remote_qubits, bsm_success_prob):
        self._num_remote_qubits = num_remote_qubits
        self._bsm_success_prob = bsm_success_prob
        super().__init__(num_qubits=2 * self.num_remote_qubits,
                         parallel=True,
                         qubit_mapping=None)

    @property
    def num_remote_qubits(self):
        return self._num_remote_qubits

    @property
    def bsm_success_prob(self):
        return self._bsm_success_prob

    def program(self):
        qubit_indices = self.get_qubit_indices(self.num_qubits)
        self.apply(InitGHZ(self.num_remote_qubits), qubit_indices[self.num_remote_qubits:])
        yield self.run()
        for i in range(self.num_remote_qubits):
            self.apply(instruction=ProbabilisticBSM(self.bsm_success_prob),
                       qubit_indices=[qubit_indices[i], qubit_indices[i + self.num_remote_qubits]],
                       output_key=f"bsm_outcome_{i}")
        yield self.run(parallel=True)

    @property
    def success(self):
        for i in range(self.num_remote_qubits):
            if not isinstance(self.output.get(f"bsm_outcome_{i}", None), BellIndex):
                return False
        return True


def tensor_product(operators):
    """Takes the tensor product of a number of operators.

    Parameters
    ----------
    operators : list or tuple of :class:`netsquid.qubits.operators.Operator`.
        Operators of which the tensor product should be taken.
        The order in which the operators are tensored is operators[0] ^ operators[1] ^ ... ^ operators[-1].

    Returns
    -------
    :class:`netsquid.qubits.operators.Operator`
        The tensor product of the operators.

    """
    operator = operators[0]
    for i in range(1, len(operators), 1):
        operator = operator ^ operators[i]
    return operator


class CorrelatedDepolarNoiseModel(DepolarNoiseModel):
    """Model for applying correlated depolarizing noise to qubit(s) on a quantum component.

    As opposed to :class:`netsquid.components.models.qerrormodels.DepolarNoiseModel`, this model depolarizes all
    partaking qubits in a correlated way. That is, either all qubits are depolarized at the same time,
    or none of the qubits are depolarized. This quantum channel acts as follows on a subspace of the Hilbert space, h,
    of d dimensions:
    rho -> (1 - p) rho + p tr_h(rho) I_d / d.

    Parameters
    ----------
    depolar_rate : float
        Probability that qubits will depolarize with time. If ``time_independent`` is False (default),
        then this is the exponential depolarizing rate per unit time [Hz].
        If True, it is a probability.
    time_independent : bool, optional
        Whether the probability of depolarizing is time independent. If True,
        it is interpreted as a probability. Default is False.

    """

    def error_operation(self, qubits, delta_time=0, **kwargs):
        """Error operation to apply to qubits.

        Parameters
        ----------
        qubits : tuple of :obj:`~netsquid.qubits.qubit.Qubit`
            Qubits to apply noise to.
        delta_time : float, optional
            Time qubits have spent on a component [ns]. Only used if
            :attr:`~netsquid.components.models.qerrormodels.DepolarNoiseModel.time_independent` is False.

        """
        if self.time_independent:
            depolar_prob = self.depolar_rate
        else:
            depolar_prob = 1. - math.exp(- delta_time * self.depolar_rate * 1e-9)
        single_qubit_operators = [ops.I, ops.X, ops.Y, ops.Z]
        operator_combinations = list(itertools.product(single_qubit_operators, repeat=len(qubits)))
        operators = [tensor_product(operator_combination) for operator_combination in operator_combinations]
        prob_nothing_happens = 1 - depolar_prob
        prob_per_operator = depolar_prob / len(operators)
        weights = [prob_per_operator] * len(operators)
        weights[0] += prob_nothing_happens
        qubits[0].qstate.stoch_operate_qubits(qubits=qubits,
                                              operators=operators,
                                              p_weights=weights)


class DepolarProbNoiseModel(DepolarNoiseModel):
    """Model for applying depolarizing noise to qubit(s) on a quantum component.

    Only difference with :class:`netsquid.components.models.qerrormodels.DepolarNoiseModel` is that the argument
    `depolar_rate` is replaced by the argument `depolar_prob`.

    Parameters
    ----------
    depolar_prob : float
        Probability that qubit will depolarize. If ``time_independent`` is False (default),
        this is the depolarizing probability per time step (i.e. per ns).
        If True, it is the absolute probability the qubit is depolarized.
    time_independent : bool, optional
        Whether the probability of depolarizing is time independent. If True,
        it is interpreted as a probability. Default is False.

    """

    def __init__(self, depolar_prob, time_independent=False, **kwargs):
        super().__init__(depolar_rate=self._prob_to_rate(depolar_prob), time_independent=time_independent, **kwargs)

    @property
    def depolar_prob(self):
        """Probability that qubit will depolarize.

        If ``time_independent`` is False (default),
        this is the depolarizing probability per time step (i.e. per ns).
        If True, it is the absolute probability the qubit is depolarized.

        """
        return self._rate_to_prob(self.properties["depolar_rate"])

    @depolar_prob.setter
    def depolar_prob(self, depolar_prob):
        self.depolar_rate = self._prob_to_rate(depolar_prob)

    @staticmethod
    def _prob_to_rate(depolar_prob):
        """Convert depolarizing probability per time step (i.e. ns) to depolarizing rate in Hz."""
        return - math.log(1 - depolar_prob) * 1E9

    @staticmethod
    def _rate_to_prob(depolar_rate):
        """Convert depolarizing rate in Hz to depolarizing probability per time step (i.e. ns)."""
        return 1 - math.exp(- depolar_rate * 1E-9)
