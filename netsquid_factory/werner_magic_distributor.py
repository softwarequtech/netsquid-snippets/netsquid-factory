from netsquid.nodes import Node
from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_magic.state_delivery_sampler import HeraldedStateDeliverySamplerFactory
import numpy as np
from netsquid.nodes.connections import Connection


class WernerMagicDistributor(MagicDistributor):
    """Magic distributor for distributing Werner states with a fixed success probability per time unit.

    Every time unit (i.e. ns), distribution of an EPR pair (i.e. |phi+> Bell state) is attempted.
    Noise in distributing this state is modeled by delivering a Werner state instead of a perfect EPR pair.
    Every attempt at entanglement distribution has a fixed success probability.
    After each failure, a new attempt is made.
    If an attempt is successful, the state is delivered at the end of that time step (so if the first attempt is
    successful, the state will be available at time = 1).
    Because the state is delivered "magically", failed attempts do not need to be simulated, greatly increasing
    efficiency.

    Parameters
    ----------
    nodes : list of :obj:`~netsquid.nodes.node.Node`
        List of nodes for which entanglement can be produced.
    prob_max_mixed : float (optional)
        Component of the Werner state that is a maximally mixed state.
        That is, the delivered state is (1 - link_depolar_prob) |phi+><phi+| + link_depolar_prob * Id / 4.
        Default is 0 (i.e. perfect EPR pair).
    success_prob : float (optional)
        Probability per time unit (i.e. ns) that the state is distributed successfully.

    """

    def __init__(self, nodes, prob_max_mixed=0, success_prob=1):
        try:
            self.prob_max_mixed = float(prob_max_mixed)
            self.success_prob = float(success_prob)
        except ValueError:
            raise TypeError("link_depolar_prob and link_success_prob must be floats, but they are "
                            f"{prob_max_mixed} and {success_prob} respectively.")
        super().__init__(delivery_sampler_factory=WernerStateSamplerFactory(prob_max_mixed=self.prob_max_mixed,
                                                                            success_prob=self.success_prob),
                         nodes=nodes, cycle_time=1, state_delay=1)


class WernerStateSamplerFactory(HeraldedStateDeliverySamplerFactory):
    """StateSamplerFactory for production of Werner states with a fixed success probability.

    Parameters
    ----------
    prob_max_mixed : float (optional)
        Component of the Werner state that is a maximally mixed state.
        That is, the delivered state is (1 - link_depolar_prob) |phi+><phi+| + link_depolar_prob * Id / 4.
        Default is 0 (i.e. perfect EPR pair).
    success_prob : float (optional)
        Probability per time unit (i.e. ns) that the state is distributed successfully.
    """

    def __init__(self, prob_max_mixed, success_prob):
        werner_state = self._create_werner_state(prob_max_mixed)

        def func_delivery(**kwargs):
            return [werner_state], [1]

        def func_success_probability(**kwargs):
            return success_prob

        super().__init__(func_delivery=func_delivery,
                         func_success_probability=func_success_probability)

    @staticmethod
    def _create_werner_state(prob_max_mixed):
        """Creates a Werner state `rho = (1 - link_depolar_prob) rho_{EPR} + link_depolar_prob (Id / 4)`.

        Parameters
        ----------
    link_depolar_prob : float (optional)
        Component of the Werner state that is a maximally mixed state.

        Returns
        -------
        :class:`numpy.array`
            Density matrix of the werner state.

        """

        epr_state = np.array(
            [[0.5, 0, 0, 0.5],
             [0, 0, 0, 0],
             [0, 0, 0, 0],
             [0.5, 0, 0, 0.5]],
            dtype=np.complex128)
        max_mixed_state = np.eye(4) / 4
        werner_state = (1 - prob_max_mixed) * epr_state + prob_max_mixed * max_mixed_state
        return werner_state


class WernerConnection(Connection):
    """Connection with :class:`factory.werner_magic_distributor.WernerMagicDistributor` attached to it.

    Parameters
    ----------
    name : str
       Name of connection for identification and display purposes.
    link_depolar_prob : float
        Used to characterize the Werner state deliverd by the magic distributor.
        This parameter is the component of the Werner state that is a maximally mixed state.
        That is, the delivered state is (1 - link_depolar_prob) |phi+><phi+| + link_depolar_prob * Id / 4.
        Default is 0 (i.e. perfect EPR pair).
    link_success_prob : float
        Probability per time unit (i.e. ns) that the state is distributed successfully by the magic distributor.
    """

    def __init__(self, name, link_depolar_prob, link_success_prob):
        super().__init__(name=name)
        self.link_depolar_prob = link_depolar_prob
        self.link_success_prob = link_success_prob
        self._magic_distributor = None

    @property
    def magic_distributor(self):
        """Magic distributor that can be used to distribute entanglement over the nodes this connection connects.

        The magic distributor is of the type :class:`factory.werner_magic_distributor.WernerMagicDistributor`.

        """
        if self._magic_distributor is None:
            if len([connected_component for connected_component in self.connected_components
                    if isinstance(connected_component, Node)]) != 2:
                raise TypeError(f"WernerConnection {self.name} is not connected to two nodes,"
                                f"and thus no magic distributor can be initialized.")
            self._magic_distributor = WernerMagicDistributor(nodes=self.connected_components,
                                                             prob_max_mixed=self.link_depolar_prob,
                                                             success_prob=self.link_success_prob)
        return self._magic_distributor

    @property
    def connected_components(self):
        """Components that are connected by this connection."""
        connected_ports = [port.connected_port for port in self.ports.values() if port.connected_port is not None]
        return [connected_port.component for connected_port in connected_ports]
