import netsquid as ns
from netsquid_netconf.builder import Builder, ComponentBuilder, NetworkBuilder
from netsquid_netconf.linker import NetworkLinker
from netsquid_netconf.netconf import _get_objects
from copy import deepcopy


class StarBuilder(Builder):
    """Creates nodes organized according to a star topology."""

    DEFAULT_NODE = {"type": "node"}
    DEFAULT_CONNECTION = {"type": "connection"}

    @classmethod
    def key(cls):
        return "star"

    @classmethod
    def build(cls, config_dict):

        if "node" not in ComponentBuilder.TYPES:
            ComponentBuilder.add_type(name="node", new_type=ns.nodes.Node)
        if "connection" not in ComponentBuilder.TYPES:
            ComponentBuilder.add_type(name="connection", new_type=ns.nodes.Connection)

        central_node_config_dict = deepcopy(config_dict.get("central_node", cls.DEFAULT_NODE))
        remote_node_config_dict = deepcopy(config_dict.get("remote_node", cls.DEFAULT_NODE))
        connection_config_dict = config_dict.get("connection", cls.DEFAULT_CONNECTION)
        connection_config_dict["connect_to"] = {}
        connection_config_dict["connect_to"].update({"node1": "central_node",
                                                     "port_name_node2": "to_central_node"
                                                     })
        num_remote_nodes = config_dict.get("num_remote_nodes", 3)

        component_builder_config_dict = {"central_node": central_node_config_dict}
        for i in range(num_remote_nodes):
            remote_node_name = f"remote_node_{i}"
            connection_name = f"connection_{i}"
            connection_config_dict["connect_to"].update({"node2": remote_node_name,
                                                         "port_name_node1": f"to_{remote_node_name}",
                                                         "connection": connection_name
                                                         })
            component_builder_config_dict[remote_node_name] = deepcopy(remote_node_config_dict)
            component_builder_config_dict[connection_name] = deepcopy(connection_config_dict)

        new_config_dict = {"network": "star_network",
                           "components": component_builder_config_dict}

        return _get_objects(config=new_config_dict,
                            builders=[ComponentBuilder, NetworkBuilder],
                            linkers=[NetworkLinker])
