import abc
from copy import deepcopy
from dataclasses import dataclass
from itertools import combinations
import logging

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
import numpy as np
from netsquid.components import INSTR_Z, INSTR_X, INSTR_Y
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes import Node, Connection
from netsquid.protocols.protocol import Protocol
from netsquid.qubits.ketstates import BellIndex

from netsquid_factory.bipartite import Fusion, QuantumRouter
from netsquid_factory.factory import GHZTeleport, GHZFactory, ProbabilisticBSM


class GHZCentralNodeProtocol(Protocol, metaclass=abc.ABCMeta):
    """Base class for protocols that distribute GHZ states using a central node.

    This base class contains functionality for
    - starting the protocol for a specific number of GHZ-state distributions,
    - determining how long it took to distribute the GHZ state,
    - determining the fidelity of the GHZ state,
    - storing distribution times and fidelities,
    - accessing connections attached to central node,
    - accessing remote nodes connected to central node through connections,
    - accessing :class:`netsquid_magic.magic_distributor.MagicDistributor`/s associated with connections.

    Parameters
    ----------
    name : str
        Name of protocol. If None, the name of the class is used.
    central_node : :class:`netsquid.nodes.node.Node`
        Needs to be connected to some remote nodes (:class:`netsquid.nodes.nodes.Node`) by
        :class:`netsquid.nodes.connections.Connection`/s.
        Each of the connections must have a :class:`netsquid_magic.magic_distributor.MagicDistributor` attached to it
        as an attribute.
        The magic distributor must be able to distribute entanglement between the central node and the remote node,
        and it is assumed that the entangled states that are delivered are EPR pairs, i.e. (|00> + |11>) / sqrt(2).
        Furthermore, each of the remote nodes must have a :class:`netsquid.components.qprocessor.QuantumProcessor`
        as quantum memory.

    """

    def __init__(self, central_node, name):
        super().__init__(name=name)
        self.central_node = central_node
        self._remote_nodes = None
        self._magic_distributors = None
        self._connections = None
        self._num = 0  # number of GHZ states to distribute
        self._last_success_time = 0
        self._distribution_times = []
        self._fidelities = []
        self._ghz_state = self._create_ghz_state(len(self.remote_nodes))
        if "num_remote_nodes" in central_node.properties:
            if len(self.remote_nodes) != central_node.properties["num_remote_nodes"]:
                raise RuntimeError(f"{central_node} has the wrong number of remote nodes.")

    def start(self, num):
        """Start the protocol.

        Parameters
        ----------
        num : int
            Number of times a GHZ state should be distributed.

        """
        self._num = num
        self._last_success_time = ns.sim_time()
        super().start()

    def stop(self):
        """Stop the protocol"""
        self._abort_deliveries()
        self._clear_callbacks()
        super().stop()

    @property
    def fidelities(self):
        """List of the fidelities of the GHZ states that have been distributed.

        `fidelities[i]` is the fidelity of the ith state that was distributed to the perfect GHZ state.

        """
        return self._fidelities

    @property
    def distribution_times(self):
        """List of the distribution times of the GHZ states that have been distributed.

        `distribution_times[i]` is the time it took to distribute the ith GHZ state (i.e. the time since the last
        state of the same run of the protocolwas distributed,
        or if there was no prior state distributed in the same run, since the protocol was last started).

        """
        return self._distribution_times

    @property
    def connections(self):
        """Connections connected to the central node."""
        if self._connections is None:
            connected_ports = [port.connected_port for port in self.central_node.ports.values()
                               if port.connected_port is not None]
            self._connections = [connected_port.component for connected_port in connected_ports
                                 if isinstance(connected_port.component, Connection)]
            if len(self._connections) == 0:
                raise ValueError("Central node must have at least one outgoing connection!")
        return self._connections

    @property
    def remote_nodes(self):
        """All nodes connected to central node."""
        if self._remote_nodes is None:
            self._remote_nodes = [port.connected_port.component
                                  for connection in self.connections
                                  for port in connection.ports.values()
                                  if port.connected_port.component != self.central_node]
            if not len(self._remote_nodes) == len(self.connections):
                raise ValueError("Each outgoing connection of the central node should have a node attached.")
            if len([remote_node for remote_node in self._remote_nodes if not isinstance(remote_node, Node)]) > 0:
                raise TypeError("Each remote node must be a Node.")
            if len([remote_node for remote_node in self._remote_nodes
                    if not isinstance(remote_node.qmemory, QuantumProcessor)]) > 0:
                raise TypeError("Each remote node must have a QuantumProcessor.")
        return self._remote_nodes

    @property
    def magic_distributors(self):
        """:class:`netsquid_magic.magic_distributor.MagicDistributor`s attached to connections as attribute."""
        if self._magic_distributors is None:
            try:
                self._magic_distributors = [connection.magic_distributor for connection in self.connections]
            except AttributeError:
                raise TypeError("Some of the connections of the central node don't have a magic distributor attached.")
            assert len(self._magic_distributors) == len(self.connections)
        return self._magic_distributors

    @abc.abstractmethod
    def _determine_fidelity(self):
        """Determine the fidelity of a state produced by the protocol to the target GHZ state.

        Returns
        -------
        float
            Fidelity of produced state to target GHZ state.

        """
        return 1

    def _add_data(self):
        """Register data about GHZ-distribution performance (i.e. fidelity and distribution time)."""

        distribution_time = ns.sim_time() - self._last_success_time
        self._last_success_time = ns.sim_time()
        self._fidelities.append(self._determine_fidelity())
        self._distribution_times.append(distribution_time)

    def _clear_callbacks(self):
        """Remove all callbacks from the magic distributors."""
        for md in self.magic_distributors:
            md.clear_all_callbacks()

    def _abort_deliveries(self):
        """Abort all delivery of entanglement by the magic distributors."""
        for md in self._magic_distributors:
            md.abort_all_delivery()

    @staticmethod
    def _create_ghz_state(num_qubits):
        """Create a GHZ state vector (ket).

        Parameters
        ----------
        num_qubits : int
            Number of qubits that should share the GHZ state.

        """
        dim = 2 ** num_qubits
        ghz = np.zeros(dim)
        ghz[0] = 1 / np.sqrt(2)
        ghz[dim - 1] = 1 / np.sqrt(2)
        return ghz


class GHZFactoryProtocol(GHZCentralNodeProtocol):
    """Protocol that distributes a GHZ state by creating it locally at a "GHZ factory" and teleporting it.

    This protocol is to be used in a scenario where there are a number of remote nodes that wish to share a GHZ state
    (i.e. (|00...0> + |11...1>) / sqrt(2)),
    and each of these nodes is connected (by a quantum channel) to a central node that is able to generate
    GHZ states locally (the "GHZ factory").
    This protocol does the following:
    1. Distribute entanglement between the factory and each of the remote nodes. Entangled states are stored in memory.
    2. As soon as entanglement is established with each remote node, a GHZ state is created locally.
    3. The GHZ state is teleported to the remote nodes by performing Bell-state measurements between qubits in the local
    GHZ state and qubits that are entangled with a remote node.
    4. Depending on the outcomes of the Bell-state measurements, Pauli corrections are performed in the remote nodes
    so that the remote nodes (in the absence of noise) share a GHZ state.
    5. The fidelity of the GHZ state and the time it took to create it are stored under
    :prop:`factory.protocols.GHZFactoryProtocol.fidelity` and
    :prop:`factory.protocols.GHZFactoryProtocol.distribution_times`.

    When starting this protocol, the number of GHZ states that should be distributed is specified.
    If still more GHZ states should be distributed after a single run of distribution finishes, a next one is started
    right away.

    Note that this protocol is nonlocal, as it operates not only the factory, but also the remote nodes
    (where it performs Pauli operations as part of teleporting the GHZ state).
    Also, the way fidelity of the distributed state is measured is both nonlocal and nonphysical.

    Parameters
    ----------
    factory : :class:`factory.factory.GHZFactory`
        GHZ factory that prepares GHZ state locally.
        Needs to be connected to some remote nodes (:class:`netsquid.nodes.nodes.Node`) by
        :class:`netsquid.nodes.connections.Connection`/s.
        Each of the connections must have a :class:`netsquid_magic.magic_distributor.MagicDistributor` attached to it
        as an attribute.
        The magic distributor must be able to distribute entanglement between the factory and the connected remote node,
        and it is assumed that the entangled states that are delivered are EPR pairs, i.e. (|00> + |11>) / sqrt(2).
        Furthermore, each of the remote nodes must have a :class:`netsquid.components.qprocessor.QuantumProcessor`
        as quantum memory.

    """

    signal_ready = "ready_to_teleport"

    def __init__(self, factory):
        if not isinstance(factory, GHZFactory):
            raise TypeError("factory must be a GHZFactory.")
        super().__init__(name="ghz_factory_protocol", central_node=factory)
        self._events = []
        self.add_signal(self.signal_ready)
        self._teleport_program = GHZTeleport(num_remote_qubits=len(self.remote_nodes),
                                             bsm_success_prob=self.central_node.properties["bsm_success_prob"])
        self._ghz_state = self._create_ghz_state(len(self.remote_nodes))

    def start(self, num):
        """Start the protocol.

        Parameters
        ----------
        num : int
            Number of times a GHZ state should be distributed.

        """
        self._events = []
        super().start(num=num)

    def run(self):
        for _ in range(self._num):
            yield from self._distribute_ghz_state()

    def _distribute_ghz_state(self):
        """Perform GHZ-state distribution (i.e. entanglement distribution followed by teleportation of GHZ state).

        This function keeps attempting GHZ-state distribution until it is successful once.

        """

        while not self._teleport_program.success:  # keep trying until teleportation is successful

            # request entanglement from magic distributors
            for i in range(len(self.remote_nodes)):
                event = self.magic_distributors[i].add_delivery({self.central_node.ID: i, self.remote_nodes[i].ID: 0})
                self._events.append(event)

            # add callbacks to magic distributors to track when entanglement distribution finishes
            for md in self.magic_distributors:
                md.add_callback(self._callback)

            # wait for signal that all entanglement is ready (sent by callback function in magic distributors)
            yield self.await_signal(sender=self, signal_label=self.signal_ready)

            # attempt creating GHZ state and teleporting it
            self.central_node.qmemory.execute_program(self._teleport_program)
            if self.central_node.qmemory.busy:
                yield self.await_program(processor=self.central_node.qmemory, await_done=True)

        self._perform_bsm_corrections()  # perform local corrections to create GHZ state on remote nodes
        self._add_data()  # log any relevant data

        # reset the program
        self._teleport_program.reset()

        # free the memory positions
        for qmem in [node.qmemory for node in self.remote_nodes]:
            qmem.set_position_used(used=False, position=0)

    def _perform_bsm_corrections(self):
        """Perform local corrections corresponding to BSM outcomes to finish teleportation."""
        for i in range(len(self.remote_nodes)):
            bsm_result = self._teleport_program.output[f"bsm_outcome_{i}"]
            qproc = self.remote_nodes[i].qmemory
            if bsm_result == BellIndex.B10:
                qproc.execute_instruction(INSTR_Z)
            elif bsm_result == BellIndex.B01:
                qproc.execute_instruction(INSTR_X)
            elif bsm_result == BellIndex.B11:
                qproc.execute_instruction(INSTR_Y)
            if qproc.busy:
                raise RuntimeError("Pauli correction for teleportation makes processor busy.")

    def _determine_fidelity(self):
        """Determine the fidelity of a state produced by the protocol to the target GHZ state.

        Returns
        -------
        float
            Fidelity of produced state to target GHZ state.

        """
        qubits = [node.qmemory.peek()[0] for node in self.remote_nodes]
        return qapi.fidelity(qubits, self._ghz_state, squared=True)

    def _callback(self, event):
        """Callback function to be called by magic distributor. Tracks when entanglement distribution finishes."""
        if event in self._events:
            self._events.remove(event)
        if len(self._events) == 0:  # this means the callback was called at least once for each magic distributor
            self.send_signal(self.signal_ready)  # notify protocol that entanglement distribution has finished
            self._clear_callbacks()  # unregister callbacks since they served their function


class GHZBipartiteProtocol(GHZCentralNodeProtocol):
    """Protocol that distributes a GHZ state by fusing Bell states distributed by a central router.

    This protocol is to be used in a scenario where there are a number of remote nodes that wish to share a GHZ state
    (i.e. (|00...0> + |11...1>) / sqrt(2)),
    and each of these nodes is connected to a central node that is able to perform Bell-state
    measurements (the "Quantum Router").
    The remote nodes must be able to perform a "fusion" operation to merge two small GHZ states into a single
    bigger GHZ state.

    When the protocol is started, entanglement generation is started on all connections.
    Then, whenever entanglement is generated successfully, the following procedure is performed:
    1. If two remote nodes are entangled with the router, and these nodes do not yet share a GHZ state,
    a Bell-state measurement in the router is attempted to create a Bell state between the two remote nodes.
    2. For each of the remote nodes that have been connected by the Bell-state measurement, if they contain two qubits
    that are each entangled to other remote nodes, a fusion operation is performed.
    3. Entanglement generation is restarted on each of the partaking remote nodes.
    4. If all remote nodes are expected to share a GHZ state,
    the fidelity of the GHZ state and the time it took to create it are stored under
    :prop:`factory.protocols.GHZBipartiteProtocol.fidelity` and
    :prop:`factory.protocols.GHZBipartiteProtocol.distribution_times`.

    Parameters
    ----------
    router : :class:`factory.bipartite.QuantumRouter`
        Router capable of performing Bell-state measurements.
        Needs to be connected to some remote nodes (:class:`factory.bipartite.BipartiteEndNode`) by
        :class:`netsquid.nodes.connections.Connection`/s.
        Each of the connections must have a :class:`netsquid_magic.magic_distributor.MagicDistributor` attached to it
        as an attribute.
        The magic distributor must be able to distribute entanglement between the router and the connected remote node,
        and it is assumed that the entangled states that are delivered are EPR pairs, i.e. (|00> + |11>) / sqrt(2).

    """

    @dataclass(frozen=True)
    class RemoteMemoryPosition:
        """Identifier for memory positions in the remote nodes connected to the router."""
        outgoing_connection_index: int
        mem_pos_remote_node: int

    @dataclass
    class RemoteEntanglement:
        """Status of entanglement between router and one of the remote nodes."""
        entanglement_ready: bool  # True if there is entanglement available right now.
        mem_pos_remote_node: int or None  # Memory position index at remote node that is (going to be) entangled

    signal_entanglement_ready = "entanglement_generated_on_connection"

    def __init__(self, router):
        if not isinstance(router, QuantumRouter):
            raise TypeError("factory must be a QuantumRouter.")
        super().__init__(central_node=router, name="ghz_bipartite_protocol")
        self._entangled_groups = []
        self._events = {}
        self._remote_entanglements = {}
        self._bsm_instruction = ProbabilisticBSM(success_probability=self.central_node.properties["bsm_success_prob"])
        self._fusion_program = Fusion()
        self.add_signal(self.signal_entanglement_ready)

    def start(self, num):
        """Start the protocol.

        Parameters
        ----------
        num : int
            Number of times a GHZ state should be distributed.

        """
        self._reset_entangled_groups()
        self._reset_events()
        self._reset_remote_entanglements()
        for md in self.magic_distributors:
            md.add_callback(self._callback)
        super().start(num=num)

    def run(self):
        """Start entanglement generation and perform BSMs / fusions whenever possible at entanglement delivery.

        First, entanglement generation is started on each outgoing connection.
        Then, the protocol waits for entanglement to finish.
        Each time this happens, it performs all possible Bell-state measurements, all possible fusions,
        and consumes/logs the target GHZ state if it has been successfully produced.

        """

        # first, start entanglement generation on each connection
        for outgoing_connection_index in self._outgoing_connection_indices:
            self._start_new_entanglement_generation(outgoing_connection_index)

        # continue until enough GHZ states have been logged
        while len(self.fidelities) < self._num:

            # wait for entanglement to be generated between the router and one of the remote nodes
            yield self.await_signal(sender=self, signal_label=self.signal_entanglement_ready)

            # perform Bell-state measurements until no longer possible, and perform fusions if required
            while True:

                # check if Bell-state measurement is possible/allowed and stop if not
                allowed_remote_memory_positions = self._get_allowed_remote_mem_pos_for_bsm()
                if not allowed_remote_memory_positions:
                    break

                # perform Bell-state measurement
                yield from self._perform_bsm(allowed_remote_memory_positions[0], allowed_remote_memory_positions[1])

                for allowed_remote_memory_position in allowed_remote_memory_positions:
                    outgoing_connection_index = allowed_remote_memory_position.outgoing_connection_index

                    # perform fusion if possible on each remote node that has been connected through BSM at router
                    if self._fusion_possible(outgoing_connection_index):
                        yield from self._perform_fusion(outgoing_connection_index)

                    # start new entanglement generation now that memory positions have been freed up in bsm and fusion
                    self._start_new_entanglement_generation(outgoing_connection_index)

            # after all Bell-state measurements and fusions have finished, check if target state is ready
            if self._ghz_state_ready:
                self._add_data()
                self._reset_entangled_groups()

    def _perform_bsm(self, remote_memory_position_1, remote_memory_position_2):
        """Perform Bell-state measurement at router.

        Performs a Bell-state measurement between two qubits at the router that are entangled with qubits at the
        remote nodes, so that an entangled state between two different remote nodes is created.
        This method
        1. performs the Bell-state measurement,
        2. performs Pauli corrections to make it as if the result of the Bell-state measurement was Phi+
           (using :meth:`factory.protocols.GHZBipartiteProtocol._perform_bsm_correction`),
        3. updates knowledge of entanglement.
        Note that the Pauli correction is nonphysical, as communication between the router and remote nodes
        is not instantaneous.
        The corrections are performed for the convenience of not having to deal with determining the expected state
        at the very end.

        Parameters
        ----------
        remote_memory_position_1 : :class:`factory.GHZBipartiteProtocol.RemoteMemoryPosition`
            Remote memory position that is entangled with router.
            Bell-state measurement will be performed on qubit at router that is entangled with this remote position.
        remote_memory_position_2 : :class:`factory.GHZBipartiteProtocol.RemoteMemoryPosition`
            Remote memory position that is entangled with router.
            Bell-state measurement will be performed on qubit at router that is entangled with this remote position.

        """
        logging.info(f"BSM: ({remote_memory_position_1.outgoing_connection_index},"
                     f"{remote_memory_position_1.mem_pos_remote_node}) and "
                     f"({remote_memory_position_2.outgoing_connection_index},"
                     f"{remote_memory_position_2.mem_pos_remote_node}) ")
        mem_pos_1 = self._get_mem_pos(remote_memory_position_1.outgoing_connection_index)
        mem_pos_2 = self._get_mem_pos(remote_memory_position_2.outgoing_connection_index)
        output, _, evt_expr = self.central_node.qmemory.execute_instruction(self._bsm_instruction,
                                                                            qubit_mapping=[mem_pos_1, mem_pos_2],
                                                                            output_key="bsm_outcome",
                                                                            physical=True)
        yield evt_expr
        bsm_outcome = output["bsm_outcome"]
        if bsm_outcome is not None:  # if BSM successful
            yield from self._perform_bsm_correction(bsm_outcome=bsm_outcome,
                                                    remote_memory_position=remote_memory_position_1)
            # update entanglement info
            entangled_group_1 = self._get_entangled_group(remote_memory_position_1)
            entangled_group_2 = self._get_entangled_group(remote_memory_position_2)
            assert len(entangled_group_1) == len(entangled_group_2) == 1  # only unentangled qubits can be connected
            new_entangled_group = entangled_group_1 | entangled_group_2
            self._entangled_groups.remove(entangled_group_1)
            self._entangled_groups.remove(entangled_group_2)
            self._entangled_groups.append(new_entangled_group)

        # update consumption of entangled links between central node and remote nodes
        self._remote_entanglements[remote_memory_position_1.outgoing_connection_index] = \
            self.RemoteEntanglement(False, None)
        self._remote_entanglements[remote_memory_position_2.outgoing_connection_index] = \
            self.RemoteEntanglement(False, None)

    def _perform_bsm_correction(self, bsm_outcome, remote_memory_position):
        """Perform Pauli correction to make it as if Bell-state measurement had Phi+ result.

        Parameters
        ----------
        bsm_outcome : :class:`netsquid.qubits.ketstates.BellIndex`
            Outcome of the Bell-state measurement.
        remote_memory_position : :class:`factory.GHZBipartiteProtocol.RemoteMemoryPosition`
            One of the remote memory positions that has been put in a Bell state by the Bell-state measurement at the
            router.
            The Pauli correction will be performed at this remote memory position.

        """
        remote_node = self._get_remote_node(remote_memory_position.outgoing_connection_index)
        if bsm_outcome in [BellIndex.B10, BellIndex.B11]:
            _, _, ev_ex = remote_node.qmemory.execute_instruction(INSTR_Z, [remote_memory_position.mem_pos_remote_node])
            yield ev_ex
        if bsm_outcome in [BellIndex.B01, BellIndex.B11]:
            _, _, ev_ex = remote_node.qmemory.execute_instruction(INSTR_X, [remote_memory_position.mem_pos_remote_node])
            yield ev_ex

    def _perform_fusion(self, outgoing_connection_index):
        """Perform fusion operation to merge entangled states held by qubits at a remote node.

        The fusion operation consists of a CNOT and a measurement.
        It allows for the merging of two GHZ states into a single GHZ state (modulo Pauli corrections).
        This method performs the fusion operation on a remote node, thereby assuming the remote node
        only has two memory positions in total.
        Furthermore, it performs Pauli corrections at other nodes to keep the total entangled state in the GHZ form,
        using :meth:`factory.protocols.GHZBipartiteProtocol._perform_fusion_corrections`.
        Finally, it updates the knowledge of entanglement.

        Note that the Pauli corrections are nonphysical, as communication between remote nodes is not instantaneous.
        The corrections are performed for the convenience of not having to deal with determining the expected state
        at the very end.

        Parameters
        ----------
        outgoing_connection_index : int
            Index of outgoing connection with the remote node that fusion should be performed at.

        """
        logging.info(f"Fusion: {outgoing_connection_index}")
        remote_node = self._get_remote_node(outgoing_connection_index)
        yield remote_node.qmemory.execute_program(self._fusion_program, qubit_mapping=[0, 1])
        measured_out_mem_pos = self.RemoteMemoryPosition(outgoing_connection_index, 1)
        yield from self._perform_fusion_corrections(fusion_outcome=self._fusion_program.output["outcome"][0],
                                                    measured_out_mem_pos=measured_out_mem_pos)

        # update knowledge of entanglement
        entangled_group_0 = self._get_entangled_group(self.RemoteMemoryPosition(outgoing_connection_index, 0))
        entangled_group_1 = self._get_entangled_group(measured_out_mem_pos)
        new_entangled_group_0 = entangled_group_0 | entangled_group_1
        new_entangled_group_0.remove(measured_out_mem_pos)
        new_entangled_group_1 = {measured_out_mem_pos}
        self._entangled_groups.remove(entangled_group_0)
        self._entangled_groups.remove(entangled_group_1)
        self._entangled_groups.append(new_entangled_group_0)
        self._entangled_groups.append(new_entangled_group_1)

    def _perform_fusion_corrections(self, fusion_outcome, measured_out_mem_pos):
        """Perform Pauli corrections to make it as if fusion had outcome 0 (thereby creating a GHZ state).

        Parameters
        ----------
        fusion_outcome : bool
            Outcome of fusion operation.
        measured_out_mem_pos : :class:`netsquid.qubits.ketstates.BellIndex`
            Remote memory position that was measured out during the fusion operation.

        """
        if fusion_outcome == 0:
            return
        remote_memory_positions_to_be_corrected = deepcopy(self._get_entangled_group(measured_out_mem_pos))
        remote_memory_positions_to_be_corrected.remove(measured_out_mem_pos)
        for remote_memory_position_to_be_corrected in remote_memory_positions_to_be_corrected:
            remote_node = self._get_remote_node(remote_memory_position_to_be_corrected.outgoing_connection_index)
            mem_pos = remote_memory_position_to_be_corrected.mem_pos_remote_node
            _, _, evt_expr = remote_node.qmemory.execute_instruction(INSTR_X, qubit_mapping=[mem_pos])
            yield evt_expr

    def _get_allowed_remote_mem_pos_for_bsm(self):
        """Get a pair of remote memory positions that can be connected through a Bell-state measurement.

        A Bell-state measurement is allowed if two remote memory positions are both entangled to the router,
        and the other remote memory positions at the same remote nodes do not share an entangled state.
        The reason for this is that otherwise, the protocol would create GHZ states with multiple qubits
        at the same node.

        Returns
        -------
        :class:`factory.protocols.GHZBipartiteProtocol.RemoteMemoryPosition`
            First remote memory position that is allowed to be part of a Bell-state measurement.
            Only if there is an allowed Bell-state measurement.
        :class:`factory.protocols.GHZBipartiteProtocol.RemoteMemoryPosition`
            Second remote memory position that is allowed to be part of a Bell-state measurement.
            Only if there is an allowed Bell-state measurement.

        """
        # first, determine remote memory positions that are entangled with the router
        available_remote_mem_pos = \
            [self.RemoteMemoryPosition(outgoing_connection_index,
                                       self._remote_entanglements[outgoing_connection_index].mem_pos_remote_node)
             for outgoing_connection_index in self._outgoing_connection_indices
             if self._remote_entanglements[outgoing_connection_index].entanglement_ready]

        # then, for each pair, determine whether they are allowed to be combined through Bell-state measurement
        # as soon as a single allowed pair is found, it is returned and the search is stopped.
        for candidates in combinations(available_remote_mem_pos, 2):
            other_mem_pos = [remote_mem_pos
                             for remote_mem_pos in self._remote_mem_pos
                             for candidate in candidates
                             if remote_mem_pos.outgoing_connection_index == candidate.outgoing_connection_index
                             if remote_mem_pos.mem_pos_remote_node != candidate.mem_pos_remote_node]
            assert len(other_mem_pos) == 2
            if self._get_entangled_group(other_mem_pos[0]) != self._get_entangled_group(other_mem_pos[1]):
                return candidates[0], candidates[1]
        return

    def _fusion_possible(self, outgoing_connection_index):
        """Determine if a fusion operation is allowed in a specific remote node.

        A fusion operation is allowed if both memory positions of the remote node share entanglement with
        memory positions at other remote nodes.
        Note that a fusion is not yet allowed when memory positions are entangled with the router.
        The reason for this is that Bell-state measurements can be probabilistic, in which case
        the entanglement could be destroyed.
        It is then favourable to keep the entangled state small until the Bell-state measurement has succeeded.

        Parameters
        ----------
        outgoing_connection_index : int
            Index of outgoing connection with the remote node that fusion could be performed at.

        Returns
        -------
        bool
            True if fusion is allowed at the indicated remote node.

        """
        logging.debug(f"check if fusion possible: {outgoing_connection_index}")
        entangled_groups = [self._get_entangled_group(remote_memory_position)
                            for remote_memory_position in self._remote_mem_pos
                            if remote_memory_position.outgoing_connection_index == outgoing_connection_index]
        if entangled_groups[0] == entangled_groups[1]:
            raise RuntimeError("Two qubits at same remote node are part of same entangled state.")
        return len(entangled_groups[0]) > 1 and len(entangled_groups[1]) > 1

    @property
    def _ghz_state_ready(self):
        """Determine whether all remote nodes share a GHZ state (which is the target state of the protocol).

        Returns
        -------
        bool
            True if a GHZ state is shared by all remote nodes.

        """
        entangled_groups_right_length = [entangled_group
                                         for entangled_group in self._entangled_groups
                                         if len(entangled_group) == self.central_node.properties["num_remote_nodes"]]
        assert len(entangled_groups_right_length) < 2
        return len(entangled_groups_right_length) == 1

    def _reset_entangled_groups(self):
        """Reset knowledge of entanglement between different remote nodes."""
        self._entangled_groups = [{remote_mem_pos} for remote_mem_pos in self._remote_mem_pos]

    def _reset_events(self):  # TODO Can _events be removed now that we have _remote_entanglements?
        """Reset events used to track entanglement generation by magic distributors."""
        self._events = {outgoing_connection_index: None
                        for outgoing_connection_index in self._outgoing_connection_indices}

    def _reset_remote_entanglements(self):
        """Reset knowledge of entanglement between remote nodes and the router."""
        self._remote_entanglements = {outgoing_connection_index: self.RemoteEntanglement(entanglement_ready=False,
                                                                                         mem_pos_remote_node=None)
                                      for outgoing_connection_index in self._outgoing_connection_indices}

    def _determine_fidelity(self):
        """Determine the fidelity of a state produced by the protocol to the target GHZ state.

        Returns
        -------
        float
            Fidelity of produced state to target GHZ state.

        """
        assert self._ghz_state_ready
        [finished_entangled_group] = [entangled_group
                                      for entangled_group in self._entangled_groups
                                      if len(entangled_group) == self.central_node.properties["num_remote_nodes"]]
        qubits = [self._get_remote_node(remote_memory_position.outgoing_connection_index).qmemory.peek(
            positions=remote_memory_position.mem_pos_remote_node
        )[0]
                  for remote_memory_position in finished_entangled_group]
        return qapi.fidelity(qubits, self._ghz_state, squared=True)

    def _start_new_entanglement_generation(self, outgoing_connection_index):
        """Start entanglement generation between router and a remote node.

        Parameters
        ----------
        outgoing_connection_index : int
            Index of outgoing connection that entanglement should be generated over.

        """

        # check if the connection does not already store entanglement, or is busy generating it:
        assert self._remote_entanglements[outgoing_connection_index] == self.RemoteEntanglement(False, None)

        # check if there is an available memory position
        available_remote_mem_pos = [remote_memory_position for remote_memory_position in self._remote_mem_pos
                                    if remote_memory_position.outgoing_connection_index == outgoing_connection_index
                                    if len(self._get_entangled_group(remote_memory_position)) == 1]
        assert len(available_remote_mem_pos) != 0

        logging.info(f"Generating entanglement on {outgoing_connection_index}")

        remote_node = self._get_remote_node(outgoing_connection_index)
        md = self._get_magic_distributor(outgoing_connection_index)
        mem_pos_remote_node = available_remote_mem_pos[0].mem_pos_remote_node
        evt = md.add_delivery({self.central_node.ID: self._get_mem_pos(outgoing_connection_index),
                               remote_node.ID: mem_pos_remote_node})
        self._events[outgoing_connection_index] = evt
        self._remote_entanglements[outgoing_connection_index] = self.RemoteEntanglement(False, mem_pos_remote_node)

    def _callback(self, event):
        """Callback function to be called by magic distributor.

        Updates knowledge of entanglement between router and remote nodes and sends a signal to notify the
        protocol that entanglement has been generated.
        Callback is called once for each node in a magic distributor.
        Therefore, the callback will be called twice each time entanglement is delivered.
        By tracking the events indicating a delivery, it is made sure the callback only performs its functions once.

        """
        outgoing_connection_indices = [index for index in self._events if self._events[index] == event]
        if not outgoing_connection_indices:  # event not registered, so callback was handled already
            return
        [outgoing_connection_index] = outgoing_connection_indices
        self._events[outgoing_connection_index] = None
        self._remote_entanglements[outgoing_connection_index].entanglement_ready = True
        self.send_signal(self.signal_entanglement_ready)

    def _get_mem_pos(self, outgoing_connection_index):
        """Get memory position at router corresponding to one of the outgoing-connection indices.

        Because the router has a single memory position per outgoing connection / remote node,
        there is a one-to-one correspondondance between outgoing connection indices and memory positions.

        Parameters
        ----------
        outgoing_connection_index : int
            Index of outgoing connection for which the memory position at the router must be obtained.

        Returns
        -------
        int
            The corresponding memory position at the router.

        """
        return outgoing_connection_index

    def _get_connection(self, outgoing_connection_index):
        """Get connection corresponding to one of the outgoing-connection indices.

        Parameters
        ----------
        outgoing_connection_index : int
            Index of outgoing connection for which the connection must be returned.

        Returns
        -------
        :class:`netsquid.nodes.connections.Connection`
            The corresponding connection between router and remote node.

        """
        return self.connections[outgoing_connection_index]

    def _get_magic_distributor(self, outgoing_connection_index):
        """Get magic distributor corresponding to one of the outgoing-connection indices.

        Magic distributor corresponding to the connection that corresponds to the outgoing-connection index.

        Parameters
        ----------
        outgoing_connection_index : int
            Index of outgoing connection for which the magic distributor must be returned.

        Returns
        -------
        :class:`netsquid_magic.magic_distributor.MagicDistributor`
            The corresponding magic distributor.

        """

        return self.magic_distributors[outgoing_connection_index]

    def _get_remote_node(self, outgoing_connection_index):
        """Get remote node corresponding to one of the outgoing-connection indices.

        Remote node that is connected to the connection that corresponds to the outgoing-connection index.

        Parameters
        ----------
        outgoing_connection_index : int
            Index of outgoing connection for which the magic distributor must be returned.

        Returns
        -------
        :class:`netsquid.nodes.node.Node`
            The corresponding node.

        """
        return self.remote_nodes[outgoing_connection_index]

    def _get_entangled_group(self, remote_memory_position):
        """Get group of entangled remote memory positions that a remote memory position belongs to.

        Parameters
        ----------
        remote_memory_position : :class:`factory.protocols.GHZBipartiteProtocol.RemoteMemoryPosition`
            Remote memory position for which the entangled group is returned.

        Returns
        -------
        set of :class:`factory.protocols.GHZBipartiteProtocol.RemoteMemoryPosition`
            Entangled group of remote memory positions.
            All memory positions in this set are expected to share a GHZ state (unless there is only one).

        """
        [entangled_group] = [entangled_group
                             for entangled_group in self._entangled_groups
                             if remote_memory_position in entangled_group]
        return entangled_group

    @property
    def _remote_mem_pos(self):
        """All remote memory positions connected to the router."""
        return [self.RemoteMemoryPosition(outgoing_connection_index, mem_pos)
                for outgoing_connection_index in self._outgoing_connection_indices
                for mem_pos in [0, 1]]

    @property
    def _outgoing_connection_indices(self):
        """All outgoing-connection indices (one for each outgoing connection / remote node)."""
        return range(self.central_node.properties["num_remote_nodes"])
