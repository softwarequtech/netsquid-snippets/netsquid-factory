from netsquid.components import DepolarNoiseModel, INSTR_MEASURE, QuantumProgram, INSTR_X, INSTR_Z
from netsquid.components.qprocessor import QuantumProcessor, PhysicalInstruction
from netsquid.nodes import Node
from netsquid.components.instructions import INSTR_CNOT
from netsquid_factory.factory import DepolarProbNoiseModel, ProbabilisticBSM


class QuantumRouter(Node):
    """Node capable of performing Bell-state measurements between any pair of qubits.

    The function of this node is to connect to remote nodes, and then connect those remote nodes by performing
    a Bell-state measurement on the corresponding entangled qubits.
    There is one qubit available per remote node that the router can connect to.

    Parameters
    ----------

    name : str
        Name of the node.
    num_remote_nodes : int
        Number of remote nodes that can connect to this router.
        This is equal to the total number of qubits in the router.
    mem_depolar_prob : float
        Probability a qubit in memory depolarizes per round of entanglement generation.
    bsm_success_prob : float
        Probability that Bell-state measurement can be performed successfully.
    bsm_depolar_prob : float
        Probability that a qubit depolarizes when it partakes in a Bell-state measurement.

    """

    def __init__(self, name, num_remote_nodes, mem_depolar_prob, bsm_success_prob, bsm_depolar_prob):
        self.add_property("num_remote_nodes", num_remote_nodes)
        self.add_property("mem_depolar_prob", mem_depolar_prob)
        self.add_property("bsm_success_prob", bsm_success_prob)
        self.add_property("bsm_depolar_prob", bsm_depolar_prob)
        super().__init__(name=name,
                         qmemory=QuantumProcessor(name=f"processor_of_{name}",
                                                  num_positions=num_remote_nodes,
                                                  memory_noise_models=DepolarProbNoiseModel(
                                                      depolar_prob=self.properties["mem_depolar_prob"]
                                                    )
                                                  )
                         )
        bsm_noise_model = DepolarNoiseModel(depolar_rate=self.properties["bsm_depolar_prob"], time_independent=True)
        self.qmemory.add_physical_instruction(PhysicalInstruction(ProbabilisticBSM(self.properties["bsm_success_prob"]),
                                                                  duration=0,
                                                                  q_noise_model=bsm_noise_model,
                                                                  apply_q_noise_after=False
                                                                  )
                                              )


class BipartiteEndNode(Node):
    """End node for bipartite GHZ-state distribution. It has two qubits, on which is can perform a "fusion" operation.

    The node is able to perform the CNOT gate and single-qubit computational-basis measurements, both with
    zero duration and no noise.
    These operations allow the node to perform :class:`netsquid_factory.bipartite.Fusion`.
    Additionally, the node is able to perform X and Z gates (also zero duation and no noise), so that any necessary
    Pauli correction can be performed.

    Parameters
    ----------

    name : str
        Name of the node.
    mem_depolar_prob : float
        Probability a qubit in memory depolarizes per round of entanglement generation.

    """

    def __init__(self, name, mem_depolar_prob):
        self.add_property("mem_depolar_prob", mem_depolar_prob)
        super().__init__(name=name,
                         qmemory=QuantumProcessor(name=f"processor_of_{name}",
                                                  num_positions=2,
                                                  memory_noise_models=DepolarProbNoiseModel(
                                                      depolar_prob=self.properties["mem_depolar_prob"]
                                                    )
                                                  )
                         )
        self.qmemory.add_physical_instruction(PhysicalInstruction(INSTR_CNOT, duration=0))
        self.qmemory.add_physical_instruction(PhysicalInstruction(INSTR_MEASURE, duration=0))
        self.qmemory.add_physical_instruction(PhysicalInstruction(INSTR_X, duration=0))
        self.qmemory.add_physical_instruction(PhysicalInstruction(INSTR_Z, duration=0))


class Fusion(QuantumProgram):
    """Operation that combines two GHZ states into one, up to some Pauli corrections.

    The fusion operation is a two-qubit operation. If both qubits are part of different GHZ states, those are combined
    into a single GHZ state, up to some possible bit-flip operations.
    It is made up of a CNOT between the two qubits, followed by a computational-basis measurement on the target qubit
    of the CNOT gate.
    If the outcome of the measurement is `1`, all the qubits that were in a GHZ state together with the target qubit
    of the CNOT gate must undergo a bit-flip correction for the total state to become an exact GHZ state.

    More precisely, the fusion operation can be described by
    FUSION_{n_1, n_1 + 1}(|GHZ>_{1, 2, ..., n_1} |GHZ>_{n_1 + 1, n_1 + 2, ..., n_1 + n_2})
        = P_{n_1 + 2} P_{n_1 + 3} ... P_{n_1 + n_2} |GHZ>_{1, 2, ..., n_1, n_1 + 2, n_1 + 3, ..., n_1 + n_2},
    where P_i = X_i if the measurement outcome was 1, and P_i = Id_i if the measurement outcome was 0.

    """

    default_num_qubits = 2

    def program(self):
        q1, q2 = self.get_qubit_indices(2)
        self.apply(INSTR_CNOT, [q1, q2])
        self.apply(INSTR_MEASURE, q2, output_key="outcome")
        yield self.run()
