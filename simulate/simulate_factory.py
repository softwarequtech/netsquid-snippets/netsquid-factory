import netsquid as ns
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes import Node
from netsquid_netconf.netconf import netconf_generator
from netsquid_netconf.builder import ComponentBuilder
import pandas as pd
import time
from netsquid_factory.factory import GHZFactory, DepolarProbNoiseModel
from netsquid_factory.protocols import GHZFactoryProtocol
from netsquid_factory.star_builder import StarBuilder
from netsquid_factory.werner_magic_distributor import WernerConnection
import argparse
import datetime


class SimpleNode(Node):
    """A node with a quantum processor (one qubit) that undergoes depolarizing memory noise.

    Parameters
    ----------
    name : str
        Name of node for display purposes.
    mem_depolar_prob : float
        Depolarizing probability per time unit (i.e. ns).

    """
    def __init__(self, name, mem_depolar_prob):
        qmem = QuantumProcessor(f"{name}_qmem", num_positions=1,
                                memory_noise_models=DepolarProbNoiseModel(depolar_prob=mem_depolar_prob),
                                fallback_to_nonphysical=True)
        super().__init__(name=name, qmemory=qmem)


def simulate_factory(num, config_path=None, output_path=None):
    """Perform simulation of GHZ-state distribution using GHZ factory.

    To distribute a GHZ state between a number of remote nodes, a :class:`factory.factory.GHZFactory` is used.
    The protocol that is performed is :class:`factory.protocols.GHZFactoryProtocol`.
    The network to perform the simulation on is set up using the NetSquid-NetConf snippet.

    Parameters
    ----------
    num : int
        Number of GHZ states to distribute (the larger this number, the smaller error margins will be).
    config_path : str
        Path of NetSquid-NetConf configuration YAML file for the network.
        If None (default), "simulate/simulate_factory.yaml" is used
        (this string is not set as default directly to enable easier use with argparse).
    output_path : str or None
        Path where the CSV file holding simulation results should be saved to.
        If None (default), "data/factory_{time_stamp}" is used,
        where {time_stamp} holds the data and time at the start of simulation (year-month-day-hour-minute-second).

    """
    config_path = "simulate/simulate_factory.yaml" if config_path is None else config_path
    if output_path is None:
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        output_path = f"data/factory_{timestamp}.csv"
    ns.set_qstate_formalism(ns.QFormalism.SPARSEDM)
    ComponentBuilder.add_type("ghz_factory", GHZFactory)
    ComponentBuilder.add_type("werner_connection", WernerConnection)
    ComponentBuilder.add_type("simple_node", SimpleNode)
    generator = netconf_generator(config_file=config_path,
                                  extra_builders=[StarBuilder])
    df = pd.DataFrame()
    for objects, config in generator:
        start_time = time.time()
        assert config["star"]["num_remote_nodes"] == config["star"]["central_node"]["properties"]["num_remote_nodes"]
        assert (config["star"]["central_node"]["properties"]["mem_depolar_prob"] ==
                config["star"]["remote_node"]["properties"]["mem_depolar_prob"])
        data = {"num_remote_nodes": int(config["star"]["num_remote_nodes"]),
                "link_success_prob": config["star"]["connection"]["properties"]["link_success_prob"],
                "link_depolar_prob": config["star"]["connection"]["properties"]["link_depolar_prob"],
                "mem_depolar_prob": config["star"]["central_node"]["properties"]["mem_depolar_prob"],
                "ghz_fidelity": config["star"]["central_node"]["properties"]["ghz_fidelity"],
                "bsm_success_prob": config["star"]["central_node"]["properties"]["bsm_success_prob"],
                "bsm_depolar_prob": config["star"]["central_node"]["properties"]["bsm_depolar_prob"]
                }
        ns.sim_reset()
        components = objects["star"]["components"]
        factory = components["central_node"]
        protocol = GHZFactoryProtocol(factory=factory)
        protocol.start(num=num)
        ns.sim_run()
        data["type"] = "factory"
        data["fidelities"] = protocol.fidelities
        data["distribution_times"] = protocol.distribution_times
        data["number_of_samples"] = len(data["fidelities"])
        df = df.append(data, ignore_index=True)
        average_fidelity = sum(data["fidelities"]) / len(data["fidelities"])
        average_distribution_time = sum(data["distribution_times"]) / len(data["distribution_times"])
        print(f"simulated GHZ distribution using factory {num} times with fidelity {round(average_fidelity, 2)} "
              f"and distribution time {round(average_distribution_time, 2)} "
              f"in {round(time.time() - start_time, 1)} seconds.")
    df.to_csv(output_path, index=False, mode="w")


def add_parser_args(parser):
    """Parse command line arguments."""
    parser.add_argument("--config", "-c", type=str, default=None, help="Config file path.")
    parser.add_argument("--output", "-o", type=str, default=None, help="Filename to save csv data to.")
    parser.add_argument("--num", "-n", type=int, default=100, help="Number of GHZ states to distribute.")
    return parser


if __name__ == "__main__":
    parser = add_parser_args(argparse.ArgumentParser(description="Perform simulation of GHZ factory."))
    args = parser.parse_args()
    simulate_factory(num=args.num, config_path=args.config, output_path=args.output)
