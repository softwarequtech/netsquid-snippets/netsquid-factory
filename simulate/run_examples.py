from simulate_factory import simulate_factory
from simulate_bipartite import simulate_bipartite


def main(num=1, show_output=False):

    if show_output:
        print(f"Simulating GHZ-state distribution using factory, {num} sample(s) per data point...")
    simulate_factory(num=num, config_path="simulate/example/simulate_factory_example.yaml",
                     output_path="simulate/example/example_data_factory.csv")
    if show_output:
        print('Done!')
        print(f"Simulating GHZ-state distribution using router, {num} sample(s) per data point...")
    simulate_bipartite(num=num, config_path="simulate/example/simulate_bipartite_example.yaml",
                       output_path="simulate/example/example_data_bipartite.csv")
    if show_output:
        print("Done!")


if __name__ == "__main__":
    main()
