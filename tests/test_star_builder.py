import unittest
import netsquid as ns
from netsquid_netconf.builder import ComponentBuilder
from netsquid_netconf.netconf import netconf_generator

from netsquid_factory.star_builder import StarBuilder


class ColoredNode(ns.nodes.Node):
    def __init__(self, name, color):
        super().__init__(name=name)
        self.color = color


class ColoredConnection(ns.nodes.Connection):
    def __init__(self, name, color):
        super().__init__(name=name)
        self.color = color


class TestStarBuilder(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ComponentBuilder.add_type("colored_node", ColoredNode)
        ComponentBuilder.add_type("colored_connection", ColoredConnection)

    @staticmethod
    def verify_output_dict(output_dict, remote_node_number):

        network = output_dict.get("network", None)
        assert isinstance(network, ns.nodes.Network)

        central_node = output_dict["components"].get("central_node", None)
        assert isinstance(central_node, ns.nodes.Node)
        assert network.get_node("central_node") is central_node
        assert "to_remote_node_0" in central_node.ports
        assert central_node.ports["to_remote_node_0"].is_connected

        remote_node = output_dict["components"].get(f"remote_node_{remote_node_number}", None)
        assert isinstance(remote_node, ns.nodes.Node)
        assert network.get_node(f"remote_node_{remote_node_number}") is remote_node
        assert "to_central_node" in remote_node.ports
        assert remote_node.ports["to_central_node"].is_connected

        connection = output_dict["components"].get(f"connection_{remote_node_number}", None)
        assert isinstance(connection, ns.nodes.Connection)
        assert network.get_connection(central_node, remote_node) is connection

    def test_one_remote_node(self):

        output_dict = StarBuilder.build(config_dict={"num_remote_nodes": 1})
        assert "remote_node_0" in output_dict["components"]
        assert "remote_node_1" not in output_dict["components"]
        self.verify_output_dict(output_dict, 0)

    def test_default_values(self):
        output_dict = StarBuilder.build(config_dict={})
        self.verify_output_dict(output_dict, 2)
        assert "remote_node_3" not in output_dict["components"]

    def test_custom_node(self):
        color_central = "red"
        color_remote = "blue"
        config_dict = {"central_node": {"type": "colored_node",
                                        "properties": {"color": color_central}
                                        },
                       "remote_node": {"type": "colored_node",
                                       "properties": {"color": color_remote}
                                       },
                       "num_remote_nodes": 6
                       }
        output_dict = StarBuilder.build(config_dict)
        assert isinstance(output_dict["components"]["central_node"], ColoredNode)
        assert output_dict["components"]["central_node"].color == color_central
        self.verify_output_dict(output_dict, 0)
        assert isinstance(output_dict["components"]["remote_node_0"], ColoredNode)
        assert output_dict["components"]["remote_node_0"].color == color_remote
        self.verify_output_dict(output_dict, 5)
        assert isinstance(output_dict["components"]["remote_node_5"], ColoredNode)
        assert output_dict["components"]["remote_node_5"].color == color_remote
        assert "remote_node_6" not in output_dict["components"]

    def test_custom_connection(self):
        color_connection = "yellow"
        config_dict = {"connection": {"type": "colored_connection",
                                      "properties": {"color": color_connection}},
                       "num_remote_nodes": 3}
        output_dict = StarBuilder.build(config_dict)
        assert isinstance(output_dict["components"]["connection_0"], ColoredConnection)
        assert output_dict["components"]["connection_0"].color == color_connection
        self.verify_output_dict(output_dict, 0)
        assert isinstance(output_dict["components"]["connection_2"], ColoredConnection)
        assert output_dict["components"]["connection_2"].color == color_connection
        self.verify_output_dict(output_dict, 2)
        assert "connection_3" not in output_dict["components"]

    def test_custom_nodes_and_connections_yaml(self):
        generator = netconf_generator(config_file="tests/test_star_builder.yaml",
                                      extra_builders=[StarBuilder])
        builder_outputs, config = next(generator)
        assert "star" in builder_outputs
        output_dict = builder_outputs["star"]

        # check central node
        assert isinstance(output_dict["components"]["central_node"], ColoredNode)
        assert output_dict["components"]["central_node"].color == "blue"

        # check first remote node and connection
        assert isinstance(output_dict["components"]["remote_node_0"], ColoredNode)
        assert output_dict["components"]["remote_node_0"].color == "yellow"
        assert isinstance(output_dict["components"]["connection_0"], ColoredConnection)
        assert output_dict["components"]["connection_0"].color == "purple"
        self.verify_output_dict(output_dict, 0)

        # check last remote node and connection
        assert isinstance(output_dict["components"]["remote_node_19"], ColoredNode)
        assert output_dict["components"]["remote_node_19"].color == "yellow"
        assert isinstance(output_dict["components"]["connection_19"], ColoredConnection)
        assert output_dict["components"]["connection_19"].color == "purple"
        self.verify_output_dict(output_dict, 19)

        # check not too many nodes and connections
        assert "remote_node_20" not in output_dict["components"]
        assert "connection_20" not in output_dict["components"]

        # test generator functionality for parameter scans works
        builder_outputs, config = next(generator)
        output_dict = builder_outputs["star"]
        assert output_dict["components"]["central_node"].color == "green"
