import unittest
import netsquid_factory.analytical as ana
import numpy as np


class TestFactoryEstimates(unittest.TestCase):

    def setUp(self):
        self.num_remote_nodes = 6
        self.link_success_prob = 1E-6  # for maximum accuracy
        self.bsm_success_prob = 1
        self.ghz_fidelity = 1
        self.mem_depolar_prob = 0
        self.link_depolar_prob = 0
        self.bsm_depolar_prob = 0

    @property
    def fidelity(self):
        return ana.factory_fidelity(num_remote_nodes=self.num_remote_nodes,
                                    link_success_prob=self.link_success_prob,
                                    ghz_fidelity=self.ghz_fidelity,
                                    mem_depolar_prob=self.mem_depolar_prob,
                                    link_depolar_prob=self.link_depolar_prob,
                                    bsm_depolar_prob=self.bsm_depolar_prob)

    @property
    def waiting_time(self):
        return ana.factory_waiting_time(link_success_prob=self.link_success_prob,
                                        bsm_success_prob=self.bsm_success_prob,
                                        num_remote_nodes=self.num_remote_nodes)

    @property
    def max_mixed_fidelity(self):
        return 1 / 2 ** self.num_remote_nodes

    def test_no_noise(self):
        assert self.fidelity == 1

    def test_nonunit_fidelity(self):
        self.ghz_fidelity = .8
        assert self.fidelity == self.ghz_fidelity

    def test_link_depolarizing(self):
        self.link_depolar_prob = .1
        assert self.fidelity < 1
        assert self.fidelity > self.max_mixed_fidelity

    def test_max_link_depolarizing(self):
        self.link_depolar_prob = 1
        assert self.fidelity == self.max_mixed_fidelity

    def test_mem_depolar_prob(self):
        self.mem_depolar_prob = .2
        assert self.fidelity < 1
        assert self.fidelity > self.max_mixed_fidelity

    def test_max_mem_depolar_prob(self):
        self.mem_depolar_prob = 1
        assert np.isclose(self.fidelity, self.max_mixed_fidelity)

    def test_bsm_depolar_prob(self):
        self.bsm_depolar_prob = .1
        assert self.fidelity < 1
        assert self.fidelity > self.max_mixed_fidelity

    def test_max_bsm_depolar_prob(self):
        self.bsm_depolar_prob = 1
        assert self.fidelity == self.max_mixed_fidelity

    def test_small_link_success_prob(self):
        self.link_success_prob = 1E-5
        assert self.waiting_time > 100

    def test_small_bsm_success_prob(self):
        self.bsm_success_prob = 1E-5
        assert self.waiting_time > 100


def test_conversion_depolar_prob_and_fidelity():
    num_qubits = 5
    assert ana.depolarizing_prob_to_fidelity(0, num_qubits) == 1
    assert ana.fidelity_to_depolarizing_prob(1, num_qubits) == 0
    assert ana.depolarizing_prob_to_fidelity(1, num_qubits) == 1 / 2 ** num_qubits
    assert ana.fidelity_to_depolarizing_prob(1 / 2 ** num_qubits, num_qubits) == 1
    depolar_value = .2
    assert np.isclose(
        ana.fidelity_to_depolarizing_prob(ana.depolarizing_prob_to_fidelity(depolar_value, num_qubits), num_qubits),
        depolar_value)
    fidelity = .8
    assert np.isclose(
        ana.depolarizing_prob_to_fidelity(ana.fidelity_to_depolarizing_prob(fidelity, num_qubits), num_qubits),
        fidelity)


class TestCheckErrors(unittest.TestCase):

    def test_check_set(self):
        num_remote_nodes = 5
        set = ()  # tuple
        with self.assertRaises(TypeError):
            ana._check_set(set, num_remote_nodes)
        set = {1, 4, num_remote_nodes + 1}  # not a subset
        with self.assertRaises(ValueError):
            ana._check_set(set, num_remote_nodes)
