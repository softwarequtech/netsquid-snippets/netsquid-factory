from netsquid.components.qmemory import QuantumMemory
from netsquid.components.qprocessor import QuantumProcessor
import netsquid as ns
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.operators as ops
import netsquid.qubits.ketstates as ketstates

from netsquid_factory.factory import InitGHZ, ProbabilisticBSM, GHZFactory, GHZTeleport, tensor_product, \
    CorrelatedDepolarNoiseModel, DepolarProbNoiseModel
import numpy as np
import unittest
import random


def ghz_dm(num_qubits):
    """Create the density matrix corresponding to a GHZ state.

    GHZ state = 1 / sqrt(2) (|00...0> + |11...1>)
    corresponding density matrix: 1 / 2 on each of the "corners" of the matrix, rest zero

    Parameters
    ----------
    num_qubits : int
        The created GHZ state corresponds to this number of qubits.

    Returns
    -------
    :obj:`numpy.array`
        Density matrix corresponding to a GHZ state of `num_remote_nodes` number of qubits.

    """
    dim = 2 ** num_qubits  # dimension of the hilbert space
    dm = np.zeros((dim, dim))
    dm[0][0] = .5
    dm[dim - 1][dim - 1] = .5
    dm[dim - 1][0] = .5
    dm[0][dim - 1] = .5
    return dm


def test_init_ghz(num_positions=4):
    """Test if :class:`factory.InitGHZ` indeed creates a GHZ state.

    Parameters
    ----------
    num_positions : int
        Number of positions involved in the :class:`factory.InitGHZ` instruction.

    """
    ns.sim_reset()
    ns.set_qstate_formalism(ns.QFormalism.KET)
    qprocessor = QuantumProcessor(name="test_init_ghz_qprocessor", num_positions=num_positions)
    qprocessor.add_instruction(InitGHZ(num_positions=num_positions), duration=0)
    qprocessor.execute_instruction(InitGHZ(num_positions=num_positions))
    ns.sim_run()
    reduced_dm = qapi.reduced_dm(qprocessor.peek(positions=list(range(num_positions))))
    assert np.isclose(reduced_dm, ghz_dm(num_positions)).all()


class TestProbabilisticBsm(unittest.TestCase):
    """Test if :class:`factory.ProbabilisticBSM` acts on qubits as expected, and fails the expected number of times."""

    @classmethod
    def setUpClass(cls):
        ns.set_qstate_formalism(ns.QFormalism.KET)

    def setUp(self):
        ns.sim_reset()
        self.qprocessor = QuantumProcessor(name="test_probabilistic_bsm_qprocessor", num_positions=2)
        self.num_repetitions = 10  # increase number to decrease probability that tests succeed on accident

    def repeated_bsm(self, success_probability, num_repetitions):
        """Perform probabilistic Bell-state measurement a couple of times and check if the outcome is expected.

        Parameters
        ----------
        success_probability : float
            Probability that deterministic BSM succeeds.
        num_repetitions : int
            Number of times BSM should be performed.

        Returns
        -------
        int
            Number of times the BSM was successful.

        """
        num_successes = 0
        for _ in range(num_repetitions):
            qubits = qapi.create_qubits(2)
            bell_index = ketstates.BellIndex(random.choice([0, 1, 2, 3]))
            bell_state = ketstates.bell_states[bell_index]
            qapi.assign_qstate(qubits=qubits,
                               qs_repr=bell_state)
            self.qprocessor.put(qubits=qubits)
            output = self.qprocessor.execute_instruction(ProbabilisticBSM(success_probability=success_probability),
                                                         output_key="bsm_outcome",
                                                         physical=False)
            outcome = output[0]["bsm_outcome"]
            if outcome is not None:
                num_successes += 1
                assert outcome == bell_index
            else:
                qstates = [qubit.qstate for qubit in qubits]
                assert qstates == [None, None]
        return num_successes

    def test_deterministic_bsm(self):
        num_successes = self.repeated_bsm(success_probability=1, num_repetitions=self.num_repetitions)
        assert num_successes == self.num_repetitions

    def test_failing_bsm(self):
        num_successes = self.repeated_bsm(success_probability=0, num_repetitions=self.num_repetitions)
        assert num_successes == 0

    def test_probabilistic_bsm(self):
        num_successes = self.repeated_bsm(success_probability=.5, num_repetitions=self.num_repetitions)
        assert num_successes < self.num_repetitions
        assert num_successes > 0


def test_ghz_teleport(num_qubits=5):
    """Perform :class:`factory.GHZTeleport` and check if it indeed teleports a GHZ state when there is preshared
    entanglement.

    Parameters
    ----------
    num_qubits : int
        Number of qubits that make up the GHZ state.

    """
    ns.sim_reset()
    ns.set_qstate_formalism(ns.QFormalism.KET)
    qprocessor = QuantumProcessor(name="test_ghz_teleport_processor",
                                  num_positions=2 * num_qubits,
                                  fallback_to_nonphysical=True)

    # prepare entangled links with remote qubits
    remote_qubits = qapi.create_qubits(num_qubits)
    local_qubits = qapi.create_qubits(num_qubits)
    for index in range(num_qubits):
        qapi.operate(remote_qubits[index], ops.H)
        qapi.operate([remote_qubits[index], local_qubits[index]], ops.CNOT)
        qprocessor.put([local_qubits[index]], [index])

    # perform the teleportation
    ghz_teleport = GHZTeleport(num_remote_qubits=num_qubits, bsm_success_prob=1)
    assert not ghz_teleport.success
    qprocessor.execute_program(ghz_teleport)
    ns.sim_run()
    assert ghz_teleport.success

    # perform corrections corresponding to Bell-state measurement outcomes
    for index in range(num_qubits):
        bsm_result = ghz_teleport.output[f"bsm_outcome_{index}"]
        bsm_result = ketstates.BellIndex(bsm_result)
        if bsm_result in [ketstates.BellIndex.B10, ketstates.BellIndex.B11]:
            qapi.operate(remote_qubits[index], ops.Z)
        if bsm_result in [ketstates.BellIndex.B01, ketstates.BellIndex.B11]:
            qapi.operate(remote_qubits[index], ops.X)

    # check if we indeed got the expected GHZ state
    assert np.isclose(qapi.reduced_dm(remote_qubits), ghz_dm(num_qubits)).all()

    # check success status is reset when the program is reset
    ghz_teleport.reset()
    assert not ghz_teleport.success

    # try again with zero success probability
    ghz_teleport = GHZTeleport(num_remote_qubits=num_qubits, bsm_success_prob=0)
    qprocessor.execute_program(ghz_teleport)
    ns.sim_run()
    assert not ghz_teleport.success


class TestFactory(unittest.TestCase):
    """Test if all required instructions and programs run successfully on the factory."""

    @classmethod
    def setUpClass(cls):
        ns.set_qstate_formalism(ns.QFormalism.KET)

    def setUp(self):
        ns.sim_reset()
        self.factory = GHZFactory(name="test_factory",
                                  num_remote_nodes=3,
                                  mem_depolar_prob=0,
                                  ghz_fidelity=1,
                                  bsm_success_prob=1,
                                  bsm_depolar_prob=0)

    def test_init_ghz(self):
        self.factory.qmemory.execute_instruction(InitGHZ(num_positions=3),
                                                 qubit_mapping=[3, 4, 5])
        ns.sim_run()
        assert np.isclose(qapi.reduced_dm(self.factory.qmemory.peek([3, 4, 5])), ghz_dm(3)).all()

    def test_probabilistic_bsm(self):
        qubits = qapi.create_qubits(2)
        bell_index = ketstates.BellIndex.PHI_PLUS
        bell_state = ketstates.bell_states[bell_index]
        qapi.assign_qstate(qubits=qubits,
                           qs_repr=bell_state)
        self.factory.qmemory.put(qubits, positions=[0, 3])
        output = self.factory.qmemory.execute_instruction(ProbabilisticBSM(success_probability=1),
                                                          qubit_mapping=[0, 3],
                                                          output_key="bsm_outcome")
        ns.sim_run()
        assert output[0]["bsm_outcome"] == ketstates.BellIndex.PHI_PLUS

    def test_ghz_teleport(self):
        qubits = qapi.create_qubits(6)
        self.factory.qmemory.put(qubits)
        ghz_teleport = GHZTeleport(num_remote_qubits=3,
                                   bsm_success_prob=self.factory.properties["bsm_success_prob"])
        self.factory.qmemory.execute_program(ghz_teleport)
        ns.sim_run()
        for i in range(3):
            bsm_outcome = ghz_teleport.output[f"bsm_outcome_{i}"]
            ketstates.BellIndex(bsm_outcome)  # will raise error if not a valid bell index


def test_bell_state_factory(mem_depolar_prob=.2, bell_state_fidelity=.9, bsm_depolar_prob=.05, duration=3):
    """Distribute a Bell state (i.e. 2-partite GHZ state) over two qubits using the GHZ factory.

    First, entanglement between two qubits in the factory and two remote qubits is created.
    Next, this is kept in memory for some time. Then, a Bell state is created locally by the factory,
    and then teleported to the remote qubits using Bell-state measurements.
    This is a simple situation, which means that we can easily calculate the exact quantum state that we expect
    to distribute over the remote qubits. This allows us to verify whether noise is accounted for correctly.

    Parameters
    ----------

    mem_depolar_prob : float
        Probability a qubit in memory depolarizes per time unit.
    bell_state_fidelity : float
        Fidelity of Bell state produced by the factory.
    bsm_depolar_prob : float
        Probability that a qubit depolarizes when it partakes in a Bell-state measurement.
    duration : int
        Number of time units entanglement is kept in memory before teleporting the local Bell state.

    Notes
    -----
    Initial Bell state is created using depolarization:
    rho_B -> (1 - p_B) rho_B + p_B I_4 / 4,
    F_B = (1 - p_B) + (p_B / 4) = 1 - (3 / 4) p_B,
    p_B = (4 / 3) (1 - F_B),
    where F_B = `bell_state_fidelity`.

    Depolarization on each of the local entangled qubits per time unit:
    rho -> (1 - p_m) rho + p_m I_2 / 2.
    After n rounds of entanglement generation:
    rho -> (1 - p_m)^n rho + (1 - (1 - p_m)^n) I_2 / 2,
    where p_m = `bsm_depolar_prob` and n = `duration`.
    Thus, the effective depolarization on each of the entangled qubits before teleportation is
    p_M = 1 - (1 - p_m)^n.

    Bell-state measurements are modelled by applying depolarizing noise on each partaking qubit before the measurement,
    rho -> (1 - p_bsm) rho + p_bsm I_2 / 2.

    In the end, all depolarizing noise can be 'moved around' (using the special property that a Bell state with
    depolarization on qubit 1 is the same as a Bell state with depolarization qubit 2) such that all noise can be
    applied to one of the remote qubits after the Bell state was teleported. This is equivalent to depolarizing
    the entire Bell state.

    Since there is one locally created Bell state, two locally entangled qubits, and four qubits partaking in BSM,
    the total effective depolarizing noise is
    p = 1 - (1 - p_B) (1 - p_M)^2 (1 - p_bsm)^4.
    The final expected state on the remote qubits is
    (1 - p) rho_B + p I_4 / 4.

    """
    ns.sim_reset()
    ns.set_qstate_formalism(ns.qubits.QFormalism.DM)  # this test only works for DM formalism!
    factory = GHZFactory(name="bell_state_factory",
                         num_remote_nodes=2,
                         bsm_success_prob=1,
                         mem_depolar_prob=mem_depolar_prob,
                         ghz_fidelity=bell_state_fidelity,
                         bsm_depolar_prob=bsm_depolar_prob)

    # create Bell states with remote qubits to be used for teleportation
    remote_qubits = qapi.create_qubits(2)
    local_qubits = qapi.create_qubits(2)
    for i in range(2):
        qapi.operate(remote_qubits[i], ops.H)
        qapi.operate((remote_qubits[i], local_qubits[i]), ops.CNOT)
        factory.qmemory.put(local_qubits[i], [i])

    # run the simulation for some time to get memory noise
    ns.sim_run(duration=duration)

    # perform teleportation
    bell_teleport = GHZTeleport(num_remote_qubits=2,
                                bsm_success_prob=factory.properties["bsm_success_prob"]
                                )
    factory.qmemory.execute_program(bell_teleport)
    ns.sim_run()

    # perform corrections corresponding to Bell-state measurement outcomes
    for index in range(2):
        bsm_result = bell_teleport.output[f"bsm_outcome_{index}"]
        bsm_result = ketstates.BellIndex(bsm_result)
        if bsm_result in [ketstates.BellIndex.B10, ketstates.BellIndex.B11]:
            qapi.operate(remote_qubits[index], ops.Z)
        if bsm_result in [ketstates.BellIndex.B01, ketstates.BellIndex.B11]:
            qapi.operate(remote_qubits[index], ops.X)

    # determine the density matrix of the delivered quantum state
    result_dm = qapi.reduced_dm(remote_qubits)

    # calculate expected dm
    p_bell_state = (4 / 3) * (1 - bell_state_fidelity)  # initial Bell-state depolarizing parameter
    p_memory = 1 - (1 - mem_depolar_prob) ** duration  # memory depolarizing parameter
    p_bsm = bsm_depolar_prob
    p = 1 - (1 - p_bell_state) * (1 - p_memory) ** 2 * (1 - p_bsm) ** 4  # effective total depolarizing parameter
    expected_dm = (1 - p) * np.outer(ketstates.b00, ketstates.b00) + p * np.eye(4, 4) / 4

    # check if resulting dm and expected dm are the same
    assert np.isclose(result_dm, expected_dm).all()

    # test if qubits can still be accessed after some time
    # (reason for this test is a bug in netsquid, see https://forum.netsquid.org/viewtopic.php?f=5&t=175 )
    ns.sim_run(duration=10)
    factory.qmemory.peek(0)


def test_tensor_product():
    product = tensor_product([ops.X, ops.Z])
    assert product == ops.X ^ ops.Z
    product = tensor_product((ops.I, ops.X, ops.Y, ops.Z))
    assert product == ops.I ^ ops.X ^ ops.Y ^ ops.Z


def test_correlated_depolar_noise_model(depolar_prob=.3):
    ns.sim_reset()
    ns.set_qstate_formalism(ns.qubits.QFormalism.DM)  # this test only works for DM formalism!
    # make state that is not too boring
    qubits = qapi.create_qubits(3)
    qapi.operate(qubits[1:], ops.H ^ ops.H)
    qapi.operate(qubits[1:], ops.CZ)
    initial_dm = qapi.reduced_dm(qubits)
    noise_model = CorrelatedDepolarNoiseModel(depolar_rate=depolar_prob, time_independent=True)
    noise_model.error_operation(qubits)
    final_dm = qapi.reduced_dm(qubits)
    dim = 2 ** len(qubits)  # dimension of Hilbert space
    expected_dm = (1 - depolar_prob) * initial_dm + depolar_prob * np.eye(dim, dim) / dim
    assert np.isclose(final_dm, expected_dm).all()


class TestDepolarProbNoiseModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.depolar_prob = .5

    def test_conversion(self):
        """Test if conversion functions are each others inverse."""
        noise_model = DepolarProbNoiseModel(depolar_prob=self.depolar_prob)
        depolar_rate = noise_model._prob_to_rate(self.depolar_prob)
        assert np.isclose(self.depolar_prob, noise_model._rate_to_prob(depolar_rate))

    def test_depolar_per_round(self, duration=1):
        """Test if the right units are used for depolarization per round (since netsquid uses ns, not "rounds")."""
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        qmem = QuantumMemory("depolar_per_round_test", num_positions=1,
                             memory_noise_models=DepolarProbNoiseModel(self.depolar_prob))
        qubit = qapi.create_qubits(1)
        qmem.put(qubit[0])
        ns.sim_run(duration=duration)
        qmem.peek()  # needed to call the noise model
        total_depolar_prob = 1 - (1 - self.depolar_prob) ** duration
        expected_dm = (1 - total_depolar_prob) * np.diag((1, 0)) + total_depolar_prob / 2 * np.eye(2)
        assert np.isclose(qapi.fidelity(qubit, expected_dm), 1)
