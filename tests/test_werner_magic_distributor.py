from netsquid_factory.werner_magic_distributor import WernerMagicDistributor, WernerConnection, \
    WernerStateSamplerFactory
import unittest
import numpy as np
from netsquid.nodes.node import Node
from netsquid.components.qmemory import QuantumMemory
import netsquid as ns
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.ketstates as ketstates


class TestWernerMagicDistributor(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.nodes = [Node("alice", qmemory=QuantumMemory("alice_qmem")),
                      Node("bob", qmemory=QuantumMemory("bob_qmem"))]
        self.memory_positions = {node.ID: 0 for node in self.nodes}

    @property
    def qubits(self):
        return [node.qmemory.peek()[0] for node in self.nodes]

    def test_passing_parameters(self, prob_max_mixed=.4, success_prob=.7):
        md = WernerMagicDistributor(nodes=self.nodes, prob_max_mixed=prob_max_mixed, success_prob=success_prob)
        assert md.prob_max_mixed == prob_max_mixed
        assert md.success_prob == success_prob
        assert isinstance(md.delivery_sampler_factory[0], WernerStateSamplerFactory)

    def test_perfect(self):
        md = WernerMagicDistributor(nodes=self.nodes, prob_max_mixed=0, success_prob=1)
        md.add_delivery(self.memory_positions)
        ns.sim_run()
        assert ns.sim_time() == 1
        assert np.isclose(qapi.fidelity(self.qubits, ketstates.b00, squared=True), 1)

    def test_small_success_prob(self):
        md = WernerMagicDistributor(nodes=self.nodes, success_prob=1E-8)
        md.add_delivery(self.memory_positions)
        ns.sim_run()
        assert ns.sim_time() > 1

    def test_max_mixed_state(self):
        md = WernerMagicDistributor(nodes=self.nodes, prob_max_mixed=1)
        md.add_delivery(self.memory_positions)
        ns.sim_run()
        max_mixed_state = np.eye(4) / 4
        assert np.isclose(qapi.fidelity(self.qubits, max_mixed_state, squared=True), 1)

    def test_imperfect_state(self, prob_max_mixed=.3):

        md = WernerMagicDistributor(nodes=self.nodes, prob_max_mixed=prob_max_mixed)
        md.add_delivery(self.memory_positions)
        ns.sim_run()
        expected_fidelity = 1 - .75 * prob_max_mixed  # (1 - p) rho + .25 p Id has F = 1 - p + .25 p = 1 - .75 p to rho
        assert np.isclose(qapi.fidelity(self.qubits, ketstates.b00, squared=True), expected_fidelity)

    def test_wrong_types(self):
        with self.assertRaises(TypeError):
            WernerMagicDistributor(nodes=self.nodes, prob_max_mixed="pmm")
        with self.assertRaises(TypeError):
            WernerMagicDistributor(nodes=self.nodes, success_prob="sp")


class TestWernerConnection(unittest.TestCase):

    def test_werner_connection(self, link_depolar_prob=.2, link_success_prob=.9):
        werner_connection = WernerConnection("test_werner_connection", link_depolar_prob=link_depolar_prob,
                                             link_success_prob=link_success_prob)
        assert werner_connection.link_depolar_prob == link_depolar_prob
        assert werner_connection.link_success_prob == link_success_prob
        assert werner_connection.connected_components == []
        with self.assertRaises(TypeError):
            werner_connection.magic_distributor
        nodes = [Node("alice", qmemory=QuantumMemory("alice_qmem")), Node("bob", qmemory=QuantumMemory("alice_qmem"))]
        nodes[0].connect_to(remote_node=nodes[1], connection=werner_connection)
        md = werner_connection.magic_distributor
        assert isinstance(md, WernerMagicDistributor)
        assert isinstance(md.delivery_sampler_factory[0], WernerStateSamplerFactory)
        assert md.delivery_sampler_factory
        assert md.prob_max_mixed == link_depolar_prob
        assert md.success_prob == link_success_prob
