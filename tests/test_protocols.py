import netsquid as ns
import numpy as np
import unittest
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes import Node, Connection
from netsquid_magic.magic_distributor import PerfectStateMagicDistributor

from netsquid_factory.bipartite import QuantumRouter, BipartiteEndNode
from netsquid_factory.factory import GHZFactory
from netsquid_factory.protocols import GHZFactoryProtocol, GHZBipartiteProtocol


class TestGHZFactoryProtocol(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.ghz_fidelity = .8
        self.num = 3

    def set_up_protocol(self, bsm_success_prob):
        factory = GHZFactory(name="ghz_factory", num_remote_nodes=2, mem_depolar_prob=0,
                             ghz_fidelity=self.ghz_fidelity, bsm_success_prob=bsm_success_prob, bsm_depolar_prob=0)
        with self.assertRaises(ValueError):
            GHZFactoryProtocol(factory)
        remote_nodes = [Node("alice", qmemory=QuantumProcessor("alice_qproc", fallback_to_nonphysical=True)),
                        Node("bob", qmemory=QuantumProcessor("bob_qproc", fallback_to_nonphysical=True))]
        magic_distributors = [PerfectStateMagicDistributor([factory, remote_nodes[0]], cycle_time=1, state_delay=1),
                              PerfectStateMagicDistributor([factory, remote_nodes[1]], cycle_time=1, state_delay=1)]
        connections = [Connection("factory-alice"), Connection("factory-bob")]
        for i in range(2):
            connections[i].magic_distributor = magic_distributors[i]
            remote_nodes[i].connect_to(remote_node=factory, connection=connections[i])
        self.protocol = GHZFactoryProtocol(factory=factory)
        self.protocol.start(num=self.num)
        ns.sim_run()
        assert self.protocol.fidelities == [self.ghz_fidelity] * self.num

    def test_instant_success(self):
        self.set_up_protocol(bsm_success_prob=1)
        assert self.protocol.distribution_times == [1] * self.num
        assert ns.sim_time() == self.num

    def test_small_success_prob(self):
        self.set_up_protocol(bsm_success_prob=.1)  # 1 / 100 success prob, since both BSMs must succeed simultaneously
        assert len(self.protocol.distribution_times) == self.num
        assert self.protocol.distribution_times != [1, 1, 1]
        assert ns.sim_time() > 3


class TestGHZBipartiteProtocol(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.num = 5

    def set_up_protocol(self, bsm_success_prob, num_remote_nodes):
        router = QuantumRouter(name="test_router", num_remote_nodes=num_remote_nodes, mem_depolar_prob=0,
                               bsm_depolar_prob=0, bsm_success_prob=bsm_success_prob)
        with self.assertRaises(ValueError):
            GHZBipartiteProtocol(router)
        remote_nodes = [BipartiteEndNode(f"remote_node_{i}", mem_depolar_prob=0) for i in range(num_remote_nodes)]
        magic_distributors = [PerfectStateMagicDistributor([router, remote_node], cycle_time=1, state_delay=1)
                              for remote_node in remote_nodes]
        connections = [Connection(f"router-{remote_node.name}") for remote_node in remote_nodes]
        for remote_node, magic_distributor, connection in zip(remote_nodes, magic_distributors, connections):
            connection.magic_distributor = magic_distributor
            remote_node.connect_to(remote_node=router, connection=connection)
        self.protocol = GHZBipartiteProtocol(router)
        self.protocol.start(num=self.num)
        ns.sim_run()
        assert np.isclose(self.protocol.fidelities, [1] * self.num).all()
        assert len(self.protocol.distribution_times) == self.num

    def test_deterministic_bsm_three_nodes(self):
        self.set_up_protocol(bsm_success_prob=1, num_remote_nodes=3)
        assert self.protocol.distribution_times == [2] * self.num

    def test_probabilistic_bsm_three_nodes(self):
        self.set_up_protocol(bsm_success_prob=.01, num_remote_nodes=3)
        assert self.protocol.distribution_times != [2] * self.num

    def test_deterministic_bsm_5_nodes(self):
        self.set_up_protocol(bsm_success_prob=1, num_remote_nodes=5)
        assert self.protocol.distribution_times == [2] * self.num
