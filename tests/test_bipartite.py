from netsquid.components import INSTR_INIT, INSTR_X, INSTR_Z
from netsquid.components.qprocessor import QuantumProcessor
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.ketstates as ketstates
from netsquid_factory.bipartite import Fusion, QuantumRouter, BipartiteEndNode
from netsquid_factory.factory import ProbabilisticBSM
import numpy as np
import netsquid as ns

ghz_state = np.zeros(8)
ghz_state[0] = 1 / np.sqrt(2)
ghz_state[7] = 1 / np.sqrt(2)


def test_fusion():
    proc = QuantumProcessor(name="test_fusion_processor", num_positions=2, fallback_to_nonphysical=True)
    fusion = Fusion()

    # test fusion on |00>
    proc.execute_instruction(INSTR_INIT, qubit_mapping=[0, 1])
    proc.execute_program(fusion, qubit_mapping=[0, 1])
    ns.sim_run()
    assert fusion.output["outcome"][0] == 0
    q0 = proc.peek(0)
    assert qapi.fidelity(q0, ketstates.s0)

    # test fusion on |10>
    proc.execute_instruction(INSTR_INIT, qubit_mapping=[0, 1])
    proc.execute_instruction(INSTR_X, qubit_mapping=[0])
    proc.execute_program(fusion, qubit_mapping=[0, 1])
    ns.sim_run()
    assert fusion.output["outcome"][0] == 1
    q0 = proc.peek(0)
    assert qapi.fidelity(q0, ketstates.s1)


def run_bipartite_three_nodes(mem_depolar_prob, bsm_depolar_prob, rounds_between_entanglement):
    """This runs a three-party version of the bipartite protocol.

    The following happens:
    1. Entanglement created between end node 0 and router
    2. Wait for some rounds
    3. Entanglement created between end node 1 and router
    4. Entanglement swap (Bell-state measurement) at router and perform single-qubit corrections accordingly
    5. Wait for some rounds
    6. Entanglement created between end node 1 and router
    7. Wait for some rounds
    8. Entanglement created between end node 2 and router
    9. Entanglement swap (Bell-state measurement) at router and perform single-qubit corrections accordingly
    10. Fusion operation at end node 1 to turn two Bell pairs into a single GHZ state

    The only sources of noise are memory depolarization and Bell-state measurement depolarization.

    Parameters
    ----------
    mem_depolar_prob : float
        Probability a qubit in memory depolarizes per round of entanglement generation.
        This is the same for the end nodes and the router.
    bsm_depolar_prob : float
        Probability that a qubit depolarizes when it partakes in a Bell-state measurement.
    rounds_between_entanglement : int
        Number of rounds each waiting step in the protocol takes.

    """
    router = QuantumRouter(name="test_quantum_router",
                           num_remote_nodes=3,
                           mem_depolar_prob=mem_depolar_prob,
                           bsm_depolar_prob=bsm_depolar_prob,
                           bsm_success_prob=1)
    remote_nodes = [BipartiteEndNode(name=f"remote_node_{i}", mem_depolar_prob=mem_depolar_prob) for i in range(3)]
    bsm = ProbabilisticBSM(success_probability=router.properties["bsm_success_prob"])
    fusion = Fusion()

    def _distribute_entanglement(remote_node_index, remote_node_position):
        """Distribute entanglement between qubit `remote_node_index` of `router` and qubit `remote_node_position` of
        `end_nodes[remote_node_index]`"""
        q_router, q_remote = qapi.create_qubits(2, no_state=True)
        qapi.assign_qstate([q_router, q_remote], ketstates.b00)
        router.qmemory.put(q_router, remote_node_index, replace=True)
        remote_nodes[remote_node_index].qmemory.put(q_remote, remote_node_position)

    # first distribute entanglement between the router and first two remote nodes, then swap it
    _distribute_entanglement(0, 0)
    ns.sim_run(duration=rounds_between_entanglement)
    _distribute_entanglement(1, 0)
    bsm_outcome_1 = router.qmemory.execute_instruction(bsm, qubit_mapping=[0, 1])
    ns.sim_run()
    bsm_outcome_1 = bsm_outcome_1[0]["last"]
    assert bsm_outcome_1 is not None
    if bsm_outcome_1 in [ketstates.BellIndex.B10, ketstates.BellIndex.B11]:
        remote_nodes[0].qmemory.execute_instruction(INSTR_Z, qubit_mapping=[0])
        ns.sim_run()
    if bsm_outcome_1 in [ketstates.BellIndex.B01, ketstates.BellIndex.B11]:
        remote_nodes[0].qmemory.execute_instruction(INSTR_X, qubit_mapping=[0])
        ns.sim_run()

    # now distribute entanglement between the router and the second two remote nodes, then swap it
    ns.sim_run(duration=rounds_between_entanglement)
    _distribute_entanglement(1, 1)
    ns.sim_run(duration=rounds_between_entanglement)
    _distribute_entanglement(2, 0)
    bsm_outcome_2 = router.qmemory.execute_instruction(bsm, qubit_mapping=[1, 2])
    ns.sim_run()
    bsm_outcome_2 = bsm_outcome_2[0]["last"]
    assert bsm_outcome_2 is not None
    if bsm_outcome_2 in [ketstates.BellIndex.B10, ketstates.BellIndex.B11]:
        remote_nodes[2].qmemory.execute_instruction(INSTR_Z, qubit_mapping=[0])
        ns.sim_run()
    if bsm_outcome_2 in [ketstates.BellIndex.B01, ketstates.BellIndex.B11]:
        remote_nodes[2].qmemory.execute_instruction(INSTR_X, qubit_mapping=[0])
        ns.sim_run()

    # finally, perform fusion at the second remote node to create GHZ state.
    remote_nodes[1].qmemory.execute_program(fusion, qubit_mapping=[0, 1])
    ns.sim_run()
    if fusion.output["outcome"][0] == 1:
        remote_nodes[2].qmemory.execute_instruction(INSTR_X, qubit_mapping=[0])
        ns.sim_run()

    ghz_qubits = [remote_node.qmemory.peek(positions=0)[0] for remote_node in remote_nodes]
    return ghz_qubits


def test_bipartite_noiseless():
    ghz_qubits = run_bipartite_three_nodes(mem_depolar_prob=0, bsm_depolar_prob=0, rounds_between_entanglement=2)
    assert np.isclose(qapi.fidelity(ghz_qubits, ghz_state), 1)


def test_bipartite_noisy(mem_depolar_prob=0.01, bsm_depolar_prob=0.1, rounds_between_entanglement=4):
    """Run three-party GHZ-state distribution in the bipartite scenario with memory noise and BSM noise.

    Setup
    -----
    Three end nodes and one central router. End nodes have qubits A, B, B' and C, where B and B' are at same end node.
    The central router has a, b and c, where a is used to connect to A, b to B/B' and c to C.
    Illustration:

    A ------- a b c --------- C
                |
                |
               B B'

    Events and noise
    ----------------
    1. A - a entanglement: no noise.
    2. Wait: one waiting time of memory noise on A and a. Noise can be moved from a to A.
    3. B - b entanglement: no noise.
    4. Swap a - b: BSM noise on a and b, after which a and b are measured. Noise can be moved from a to A and b to B.
    5. Wait: one waiting time of memory noise on A and B.
    6. B' - b entanglement: no noise.
    7. Wait: one waiting time of memory noise on A, B, B' and b. Noise can be moved from b to B'.
    8. C - c entanglement: no noise.
    9. Swap b - c: BSM noise on b and c, after which b and c are measured. Noise can be moved from b to B' and c to C
    10. Fusion B - B': noise noise. B' is measured out. Noise can be moved from B to A and B' to C.

    Total noise
    -----------
    By moving noise between the two qubits that share a Bell state as explained above, the total noise per qubit
    that is aggrevated is as follows:
    - A: six waiting times of memory noise and two BSM noises,
    - B: no noise,
    - C: two waiting times of memory noise and two BSM noises.

    If the amount of memory noise per round, `mem_depolar_prob`, is p_m, then the amount of memory noise per
    `rounds_between_entanglement` is p_M = 1 - (1 - p_m) ** `rounds_between_entanglement`.
    Furthermore, we denote the BSM noise, `bsm_depolar_prob`, by p_bsm.
    This gives as depolarizing probability per qubits
    - A: p_A = 1 - (1 - p_M) ** 6 * (1 - p_bsm) ** 2,
    - B: p_B = 0,
    - C: p_C = 1 - (1 - p_M) ** 2 * (1 - p_bsm) ** 2.

    Fidelity
    --------
    Denoting a depolarizing channel acting on qubit q with depolarizing probability p by D(q, p), the final quantum
    state is
    dm = D(A, p_A) D(B, p_B) D(C, p_C) |GHZ_ABC><GHZ_ABC|. (The depolarizing channels commute.)
    After a single qubit in a GHZ state is depolarized, the fidelity drops by a factor 4.
    Subsequent depolarizing channels on other qubits drop the fidelity by a factor 2,
    except when the last undepolarized qubit is depolarized; this does not lower change the state any further.
    This allows us to write down the final fidelity as
    F = (1 - p_A)(1 - p_B)(1 - p_C)
        + (p_A (1 - p_B)(1 - p_C) + p_B (1 - p_A)(1 - p_C) + p_C (1 - p_B)(1 - p_A)) / 4
        + (p_A p_B (1 - p_C) + p_A p_C (1 - p_B) + p_C p_B (1 - p_A) + p_A p_B p_C) / 8.

    Parameters
    ----------
    mem_depolar_prob : float
        Probability a qubit in memory depolarizes per round of entanglement generation.
        This is the same for the end nodes and the router.
    bsm_depolar_prob : float
        Probability that a qubit depolarizes when it partakes in a Bell-state measurement.
    rounds_between_entanglement : int
        Number of rounds each waiting step in the protocol takes.

    """
    ns.sim_reset()
    ns.set_qstate_formalism(ns.QFormalism.DM)
    ghz_qubits = run_bipartite_three_nodes(mem_depolar_prob=mem_depolar_prob, bsm_depolar_prob=bsm_depolar_prob,
                                           rounds_between_entanglement=rounds_between_entanglement)
    pM = 1 - (1 - mem_depolar_prob) ** rounds_between_entanglement
    pbsm = bsm_depolar_prob
    pA = 1 - (1 - pM) ** 6 * (1 - pbsm) ** 2
    pB = 0
    pC = 1 - (1 - pM) ** 2 * (1 - pbsm) ** 2
    prob_no_depolar = (1 - pA) * (1 - pB) * (1 - pC)
    prob_one_depolar = pA * (1 - pB) * (1 - pC) + pB * (1 - pA) * (1 - pC) + pC * (1 - pB) * (1 - pA)
    prob_two_depolar = pA * pB * (1 - pC) + pA * pC * (1 - pB) + pC * pB * (1 - pA)
    prob_three_depolar = pA * pB * pC
    assert np.isclose(prob_no_depolar + prob_one_depolar + prob_two_depolar + prob_three_depolar, 1)
    expected_fidelity = prob_no_depolar + prob_one_depolar / 4 + prob_two_depolar / 8 + prob_three_depolar / 8
    simulated_fidelity = qapi.fidelity(ghz_qubits, ghz_state, squared=True)
    assert np.isclose(expected_fidelity, simulated_fidelity)
