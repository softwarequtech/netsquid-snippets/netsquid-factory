from netsquid.components import DepolarNoiseModel
from netsquid.components.qmemory import QuantumMemory
import netsquid.qubits.qubitapi as qapi
import netsquid as ns


depolar_rate = 0.1  # error does not occur when set to 0
ns.set_qstate_formalism(ns.QFormalism.DM)  # only error if ensemble-based formalisms

qproc = QuantumMemory("test", 1, memory_noise_models=DepolarNoiseModel(depolar_rate))
q, = qapi.create_qubits(1)
qproc.put(qubits=q, positions=0)
qapi.discard(q)
ns.sim_run(duration=100)
qproc.peek(0)
