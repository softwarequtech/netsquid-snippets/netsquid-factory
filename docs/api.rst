API Documentation
-----------------

Below are the modules of this package.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/factory.rst
   modules/bipartite.rst
   modules/protocols.rst
   modules/star_builder.rst
   modules/werner_magic_distributor.rst
   modules/analytical.rst
